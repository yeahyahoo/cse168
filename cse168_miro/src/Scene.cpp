#include "Miro.h"
#include "Scene.h"
#include "Camera.h"
#include "Image.h"
#include "Console.h"
#include "PFMLoader.h"

#include "Triangle.h"

#include <omp.h>

#include <iostream>
#include <cstdio>
#include <ctime>

#include "Light.h"
#include "AreaLight.h"
#include "SphereLight.h"

Scene * g_scene = 0;
Vector3 * Scene::envTexture = 0;
long unsigned int Scene::num_bvh_intersect = 0;
long unsigned int Scene::num_seconday_rays = 0;

void
Scene::openGL(Camera *cam)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    cam->drawGL();
	if(m_bvh.usingBVH()){
		//m_bvh.drawBoxes();
	}
    // draw objects
    for (int i = 0; i < m_objects.size(); ++i)
        m_objects[i]->renderGL();
	
    glutSwapBuffers();
}

void
Scene::preCalc()
{
    Objects::iterator it;
    for (it = m_objects.begin(); it != m_objects.end(); it++)
    {
        Object* pObject = *it;
        pObject->preCalc();
    }
    Lights::iterator lit;
    for (lit = m_lights.begin(); lit != m_lights.end(); lit++)
    {
        Light* pLight = *lit;
        pLight->preCalc();
    }

    m_bvh.build(&m_objects);
	
	std::cout<< "Num_BVHNodes:\t" << BVH::num_BVHNodes << std::endl;
	std::cout<< "Num_leafNodes:\t" << BVH::num_BVHLeaves << std::endl; 
}
void Scene::resetCounters()
{
	AABB::resetCounters();
	Triangle::resetCounters();
	num_bvh_intersect = 0;
	num_seconday_rays = 0;
}

void
Scene::raytraceImage(Camera *cam, Image *img)
{
	resetCounters();    
	std::clock_t start;
	double duration;
	start = std::clock();

	int height, width;
	height = img->height();
	width = img->width();

	int strait = 10;
	float straitInterval = 1.0/strait;
	
    // loop over all pixels in the image
    for (int j = 0; j < height; ++j)
    {	
		omp_set_dynamic(0);
		#pragma omp parallel for num_threads(8)
		for (int i = 0; i < width; ++i)
		{
			if(true){ //JOEY
			RayPacket rayPacket;
			int counter = 0;
			Vector3 shadeResult(0);
			for(int k = 0; k < strait; k++)
			{
				for(int l = 0; l < strait; l++)
				{					
					Ray ray;
					float x = (i-0.5f) + (straitInterval*(l + rand()/(float)RAND_MAX));
					float y = (j-0.5f) + (straitInterval*(k + rand()/(float)RAND_MAX));					
					rayPacket.rayInfo[counter] = cam->eyeRay(x, y, width, height); // initialize sample rays
					rayPacket.hitInfo[counter].t = MIRO_TMAX; // initialize the t to max value
					rayPacket.hitMask[counter] = true; // going into bvh refers to if this should be check, leaveing bvh refers to if we got a hit
					counter++;
					if(counter == 4)
					{
						tracePacket(rayPacket);
						for(;counter-- > 0;)
						{						
							if(rayPacket.hitMask[counter]){
								shadeResult += rayPacket.hitInfo[counter].material->shade(rayPacket.rayInfo[counter], rayPacket.hitInfo[counter], *this);
							}else{
								shadeResult += findEnvCoords(rayPacket.rayInfo[counter].d);											
							}
						}						
					}
				}
			}
			
			shadeResult /= (strait*strait);
			img->setPixel(i, j, shadeResult); 
			
			}else{// JOEY
			
			Vector3 shadeResult(0);
			HitInfo hitInfo;
			Ray ray;   			
			if(strait > 1){
			for(int k = 0; k < strait; k++)
			{
				for(int l = 0; l < strait; l++)
				{
					float x = (i-0.5f) + (straitInterval*(l + rand()/(float)RAND_MAX));
					float y = (j-0.5f) + (straitInterval*(k + rand()/(float)RAND_MAX));
					ray = cam->eyeRay(x, y, width, height);			
					if (trace(hitInfo, ray))
					{
			            shadeResult += hitInfo.material->shade(ray, hitInfo, *this); 		
						
					}else{
						shadeResult += findEnvCoords(ray.d);								
					}
				}
			}
			shadeResult /= (strait*strait);
			img->setPixel(i, j, shadeResult); 
			}else{
				ray = cam->eyeRay(i, j, width, height);			
				if (trace(hitInfo, ray))
				{
		            shadeResult = hitInfo.material->shade(ray, hitInfo, *this); 		
					img->setPixel(i, j, shadeResult); 
				}else{
					shadeResult = findEnvCoords(ray.d);		
					img->setPixel(i, j, shadeResult);
				}		
			
			}		
			}//JOEY
			
		}
        img->drawScanline(j);
        glFinish();
        printf("Rendering Progress: %.3f%%\r", j/float(img->height())*100.0f);
        fflush(stdout);
    }
    
    printf("Rendering Progress: 100.000%\n");
	duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
	printf("completed in %f seconds\n", duration);	

	std::cout<< "Num_rays_per_pixel:\t" << strait*strait << std::endl;
	
	std::cout<< "Num_bvh_intersections:\t" << Scene::num_bvh_intersect << std::endl;	

	std::cout<< "Num_ray_box:\t" << AABB::num_ray_box<<std::endl;
	std::cout<< "Num_ray_tri:\t" << Triangle::num_ray_tri<<std::endl;
    debug("done Raytracing!\n");	
}

bool
Scene::trace(HitInfo& minHit, const Ray& ray, float tMin, float tMax) const
{
	num_bvh_intersect++;
    return m_bvh.intersect(minHit, ray, tMin, tMax);
}

void
Scene::tracePacket(RayPacket& packet, float tMin, float tMax) const
{
	num_bvh_intersect++;
	m_bvh.packetIntersect(packet, tMin, tMax);	
}

static int maxDepth = 10;
static int rayDepth = 0;
// should only be called by secondary rays
// such as reflect, refract, diffuseRays etc...has a maximum depth allowed
Vector3 Scene::trace2nd(const Ray& reflectRay) const
{        
    HitInfo hitInfo;
    Vector3 L = Vector3(0.0f, 0.0f, 0.0f);            

    rayDepth++;
    if(rayDepth < maxDepth) 
    {
        if (trace(hitInfo, reflectRay, epsilon))
            L += hitInfo.material->shade(reflectRay, hitInfo, *this); 
    }
    rayDepth--;

    return L;
}

void Scene::loadEnvironmentMap(char *fileName){

	envTexture = readPFMImage(fileName,&envWidth,&envHeight);
}

Vector3 Scene::findEnvCoords(Vector3 dir) const {
	if(envTexture == NULL) return Vector3();
	float r =  (1/PI) * acos(dir.z)/ sqrt(dir.x * dir.x + dir.y *dir.y);
	float u = dir.x * r; 
	float v = dir.y * r;

	float n_u = (u + 1)/2;
	float n_v = (v + 1)/2;

	int mapWidth = int( n_u *envWidth);
	int mapHeight = int(n_v * envHeight);

	return envTexture[envWidth * mapHeight + mapWidth];
}
