#ifndef _LIGHT_H_
#define _LIGHT_H_

#include "Ray.h"
#include <math.h>
#include <vector>
#include "Vector3.h"

class Light
{
public:
    void setPosition(const Vector3& v)  {m_position = v;}
    void setColor(const Vector3& v)     {m_color = v;}
    void setWattage(float f)            {m_wattage = f;}
    	
    float wattage() const               {return m_wattage;}
    const Vector3 & color() const       {return m_color;}
    const Vector3& position() const     {return m_position;}

	//virtual float calcShadow(const Ray& ray, Scene scene&, float distance2) const;
    //virtual bool checkShadow(const Ray& ray, Scene scene&, float distance2) const;

	virtual void getStratifiedSamples(Ray* ray, int& u_samples, int& v_samples) = 0;
	virtual void getSample(Ray& ray) = 0;

	virtual void getStratifiedShadowRay(const Vector3 hitpos, Ray* ray, int& u_samples, int& v_samples) = 0;
	virtual void getShadowRay(const Vector3 hitpos, Ray& ray) = 0;

	virtual Vector3 positionShadow(Vector3 hitpos) = 0;

	

    void preCalc() {} // use this if you need to

protected:
    Vector3 m_position;
    Vector3 m_color;	
    float m_wattage;
};

typedef std::vector<Light*> Lights;

#endif // CSE168_POINTLIGHT_H_INCLUDED
