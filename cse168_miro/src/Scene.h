#ifndef CSE168_SCENE_H_INCLUDED
#define CSE168_SCENE_H_INCLUDED

#include "Miro.h"
#include "Object.h"
#include "Ray.h"
#include "BVH.h"
#include "Light.h"
#include "photonmap.h"

class Camera;
class Image;

class Scene
{
public:
    void addObject(Object* pObj)        {m_objects.push_back(pObj);}
    const Objects* objects() const      {return &m_objects;}

    void addLight(Light* pObj)     {m_lights.push_back(pObj);}
    const Lights* lights() const        {return &m_lights;}

	Photon_map* globalPhoton() {return m_global_map;}
	Photon_map* causticPhoton() {return m_caustic_map;}
	

    void preCalc();
    void openGL(Camera *cam);

    void raytraceImage(Camera *cam, Image *img);
    bool trace(HitInfo& minHit, const Ray& ray,
               float tMin = 0.0f, float tMax = MIRO_TMAX) const;
	void tracePacket(RayPacket& packet,
			   float tMin = 0.0f, float tMax = MIRO_TMAX) const;
	Vector3 trace2nd(const Ray& reflectRay) const;

    
	void loadEnvironmentMap(char *fileName);
	Vector3 findEnvCoords(Vector3 dir) const;    
	void resetCounters();
	
	static Vector3 *envTexture;

	///////////////////COUNTERS///////////////////	
	static long unsigned int num_bvh_intersect;
	static long unsigned int num_seconday_rays;
	


protected:
    Objects m_objects;
    BVH m_bvh;
    Lights m_lights;
	Photon_map* m_global_map;
	Photon_map* m_caustic_map;
    
private:
	bool useEnvironmentMap;
	int envWidth;
	int envHeight;
	
};

extern Scene * g_scene;

#endif // CSE168_SCENE_H_INCLUDED
