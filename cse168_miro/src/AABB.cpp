#include "AABB.h"
#include "Ray.h"
#include "Console.h"
#include "TriangleMesh.h"
#include "Triangle.h"

#include <smmintrin.h>
#include <omp.h>

using namespace std;

long unsigned int AABB::num_ray_box = 0;
AABB::AABB()
{
	a = Vector3(FLT_MAX); //min
	b = Vector3(-FLT_MAX); //max
	isLeaf = false;
	
}
AABB::AABB(Vector3 corner_a, Vector3 corner_b)
{
	a = corner_a;
	b = corner_b;
	isLeaf = false;
	objects = new Objects;
	
}
AABB::AABB(Objects* objList)
{
	Vector3 minCorner = Vector3(FLT_MAX);
	Vector3 maxCorner = Vector3(-FLT_MAX);
	Vector3 currMin, currMax;

	for(unsigned int i = 0; i < objList->size(); ++i )
	{
		objList->at(i)->getAABB(currMin, currMax);

		minCorner.x = min( currMin.x , minCorner.x);
		minCorner.y = min( currMin.y , minCorner.y);
		minCorner.z = min( currMin.z , minCorner.z);

		maxCorner.x = max( currMax.x , maxCorner.x);
		maxCorner.y = max( currMax.y , maxCorner.y);
		maxCorner.z = max( currMax.z , maxCorner.z);
	}	
	a = minCorner;
	b = maxCorner;

	isLeaf = (objList->size() <= 1)?true:false;
	objects = new Objects;

}

AABB::~AABB()
{

}

float AABB::getSurfaceArea()
{
	float x = b.x-a.x;
	float y = b.y-a.y;
	float z = b.z-a.z;
	return 2*(x*y+x*z+y*z);
}
float AABB::getVolume()
{
	float x = b.x-a.x;
	float y = b.y-a.y;
	float z = b.z-a.z;
	return x*y*z;
}
Vector3 AABB::getCenter()
{
	return (a+b)/2;
}
static int renderGLDepth = 0;
void
AABB::renderGL()
{
	renderGLDepth++;
	if(renderGLDepth == 4  || this->isLeaf){ // && abs(a.x)<3 && abs(b.x) <3 && abs(a.y)<3 && abs(b.y) <3 && abs(a.z)<3 && abs(b.z) <3){
        Vector3 ptA( a );
        Vector3 ptB( a.x, b.y, a.z );
        Vector3 ptC( b.x, b.y, a.z );
        Vector3 ptD( b.x, a.y, a.z );
        Vector3 ptE( b.x, a.y, b.z );

        Vector3 ptF( b );
        Vector3 ptG( a.x, b.y, b.z );
        Vector3 ptH( a.x, a.y, b.z );

        glBegin(GL_QUADS); // order: top left pt, top right pt, bottom right pt, bottom left pt

            // front face
            glVertex3f( ptG.x, ptG.y, ptG.z );
            glVertex3f( ptF.x, ptF.y, ptF.z );
            glVertex3f( ptE.x, ptE.y, ptE.z );
            glVertex3f( ptH.x, ptH.y, ptH.z );

            // back face
            glVertex3f( ptC.x, ptC.y, ptC.z );
            glVertex3f( ptB.x, ptB.y, ptB.z );
            glVertex3f( ptA.x, ptA.y, ptA.z );
            glVertex3f( ptD.x, ptD.y, ptD.z );

            // right face
            glVertex3f( ptF.x, ptF.y, ptF.z );
            glVertex3f( ptC.x, ptC.y, ptC.z );
            glVertex3f( ptD.x, ptD.y, ptD.z );
            glVertex3f( ptE.x, ptE.y, ptE.z );

            // left face
            glVertex3f( ptB.x, ptB.y, ptB.z );
            glVertex3f( ptG.x, ptG.y, ptG.z );
            glVertex3f( ptH.x, ptH.y, ptH.z );
            glVertex3f( ptA.x, ptA.y, ptA.z );

            // bottom face
            glVertex3f( ptH.x, ptH.y, ptH.z );
            glVertex3f( ptE.x, ptE.y, ptE.z );
            glVertex3f( ptD.x, ptD.y, ptD.z );
            glVertex3f( ptA.x, ptA.y, ptA.z );

            // top face
            glVertex3f( ptB.x, ptB.y, ptB.z );
            glVertex3f( ptC.x, ptC.y, ptC.z );
            glVertex3f( ptF.x, ptF.y, ptF.z );
            glVertex3f( ptG.x, ptG.y, ptG.z );

        glEnd();
	}

	//std::cout<<"Min "<<a<<"    Max"<<b<<endl;
	if(objects != NULL)
	{
		for(unsigned int i = 0; i < objects->size();i++)
		{
			(*objects)[i]->renderGL();
		}
	}
	renderGLDepth--;
	/*	if(objects->size() == 2)
	{
		(((AABB*)objects)[0]).renderGL();
		(((AABB*)objects)[1]).renderGL();
	}
	*/
}

bool
AABB::intersect(HitInfo& result, const Ray& ray,float tMin, float tMax)
{

	if(!this->isLeaf){		
		
		HitInfo left_info;
		HitInfo right_info;
		float left_t;
		float right_t;
		bool left_hit = false;
		bool right_hit = false;
		bool hit = false;
		left_info.t = result.t;
		right_info.t = result.t;

		left_hit = ((AABB*)objects->at(0))->intersectBox(ray, left_t);
		right_hit = ((AABB*)objects->at(1))->intersectBox(ray, right_t);

		if(left_hit && right_hit)
		{
			if(left_t < right_t)
			{			
				if(objects->at(0)->intersect( left_info, ray, tMin, tMax))
				{
					if( left_info.t < result.t ){
						result = left_info;
						hit = true;
					}
				}
				if(right_t < result.t )
				{
					right_info.t = result.t;
					if(objects->at(1)->intersect( right_info, ray, tMin, tMax))
					{
						if( right_info.t < result.t) 
						{
							result = right_info;
							hit = true;
						}
					}
				}
			}else{				
				if(objects->at(1)->intersect( right_info, ray, tMin, tMax))
				{
					if( right_info.t < result.t)
					{
						result = right_info;
						hit = true;
					}
				}
				if(left_t < result.t )
				{
					left_info.t = result.t;
					if(objects->at(0)->intersect( left_info, ray, tMin, tMax))
					{
						if(left_info.t < result.t)
						{
							result = left_info;
							hit = true;
						}
					}
				}			
			}
		}else if(left_hit){
			if(objects->at(0)->intersect( left_info, ray, tMin, tMax))
			{
				if( left_info.t < result.t ){
					result = left_info;
					hit = true;
				}
			}
		}else if(right_hit){
			if(objects->at(1)->intersect( right_info, ray, tMin, tMax))
			{
				if( right_info.t < result.t)
				{
					result = right_info;
					hit = true;
				}
			}
		}
		
		return hit;

	}else{

		///// if this box is a leaf, then all children are triangles/spheres
		bool gotHit = false;
		HitInfo info;
		for(unsigned int i = 0; i < objects->size(); ++i){

			if(objects->at(i)->intersect( info, ray, tMin, tMax)){
				if(info.t < result.t || !gotHit){
					result = info;
					gotHit = true;
				}
			}
		}
		return gotHit;
	}	
} 

void 
AABB::packetIntersect(	HitInfo result[4], 
						Ray rayInfo[4],
						bool mask[4],
						float tMin, float tMax)
{

	if(!isLeaf)
	{
		HitInfo info[4];		

		bool mask_left[4] = {false,false,false,false};
		bool mask_right[4] = {false,false,false,false};		

		float left_t[4];
		float right_t[4];	
		
		float left_check[4];
		float right_check[4];	

		float left_dist = 0;
		float right_dist = 0;
		float left_count = 0;
		float right_count = 0;
		int i;


		((AABB*)objects->at(0))->fourRayIntersectBox(rayInfo, left_t,left_check);
		((AABB*)objects->at(1))->fourRayIntersectBox(rayInfo, right_t,right_check);

		for ( i = 4 ; i-- > 0 ; ){
			if( mask[i] && left_check[i] != 0)
			{
				mask_left[i] = true;
				left_dist += left_t[i];
				left_count++;				
			}
			if( mask[i] && right_check[i] != 0)
			{
				mask_right[i] = true;
				right_dist += right_t[i];
				right_count++;				
			}
			mask[i] = mask_right[i] && mask_left[i];
		}			

		if(left_count+right_count == 0) return;

		left_dist /= (left_count)?left_count:1;
		right_dist /= (right_count)?right_count:1;		
		
		info[0].t = MIRO_TMAX;
		info[1].t = MIRO_TMAX;
		info[2].t = MIRO_TMAX;
		info[3].t = MIRO_TMAX;

		if(right_count == 0) // intersect left only
		{	
			((AABB*)objects->at(0))->packetIntersect( info, rayInfo, mask_left, tMin, tMax);

			for ( i = 4 ; i-- > 0; ){
				if(mask_left[i] && info[i].t < result[i].t){						
					result[i] = info[i];					
					mask[i] = true;
				}else{
					mask[i] = mask_left[i];
				}				
			} 
		} 
		else if(left_count == 0) // intersect right only
		{

			((AABB*)objects->at(1))->packetIntersect( info, rayInfo, mask_right, tMin, tMax);
			for ( i = 4 ; i-- > 0 ; ){
				if(mask_right[i] && info[i].t < result[i].t){
					result[i] = info[i];						
					mask[i] = true;
				}else{
					mask[i] = mask_right[i];
				}				
			} 
		}
		else if(left_dist<right_dist)
		{
			((AABB*)objects->at(0))->packetIntersect( info, rayInfo, mask_left, tMin, tMax);
			for ( i = 4 ; i-- > 0 ; ){				
				if(mask_left[i] && info[i].t < result[i].t)
				{
					result[i] = info[i];	
					mask[i] = true;
					if(info[i].t < right_t[i])
					{
						mask_right[i] = false;
					}
				}else{
					mask[i] = mask_left[i];					
				}
			}
			info[0].t = MIRO_TMAX;
			info[1].t = MIRO_TMAX;
			info[2].t = MIRO_TMAX;
			info[3].t = MIRO_TMAX;

			((AABB*)objects->at(1))->packetIntersect( info, rayInfo, mask_right, tMin, tMax);
			for ( i = 4 ; i-- > 0 ; )
			{				
				if(mask_right[i] && info[i].t < result[i].t)
				{
					result[i] = info[i];
					mask[i] = true;
				}
			}			
		}
		else
		{			
			((AABB*)objects->at(1))->packetIntersect( info, rayInfo, mask_right, tMin, tMax);
			for ( i = 4 ; i-- > 0; )
			{				
				if( mask_right[i] && info[i].t < result[i].t){
					result[i] = info[i];	
					mask[i] = true;
					if(info[i].t < left_t[i])
					{
						mask_left[i] = false;
					}
				}else{
					mask[i] = mask_right[i];
				}				
			} 
			info[0].t = MIRO_TMAX;
			info[1].t = MIRO_TMAX;
			info[2].t = MIRO_TMAX;
			info[3].t = MIRO_TMAX;

			((AABB*)objects->at(0))->packetIntersect( info, rayInfo, mask_left, tMin, tMax);
			for ( i = 4 ; i-- > 0; ){				
				if( mask_left[i] && info[i].t < result[i].t){
					result[i] = info[i];											
					mask[i] = true;
				}
			}
		}		

	}else{		
		///// if this box is a leaf, then all children are triangles/spheres

		bool gotHit = false;		
		HitInfo info;

		for(unsigned int i = 0; i < 4; ++i)
		{			
			gotHit = false;
			for(unsigned int j = 0; j < objects->size() ; j++)
			{				
				if(mask[i]){
					if(objects->at(j)->intersect( info, rayInfo[i], tMin, tMax)){
						if(info.t < result[i].t){
							result[i] = info;	
							gotHit = true;
						}
					}
				}			
			}
			mask[i] = gotHit;

		}
	}		
}


void 
AABB::packetIntersect(RayPacket& rayPacket, std::vector<bool>& mask, float tMin, float tMax)
{	
	if(!isLeaf)
	{		
		std::vector<bool> left_mask;		
		std::vector<bool> right_mask;		
		std::vector<float> left_t;
		std::vector<float> right_t;
		left_mask.resize(mask.size());		
		right_mask.resize(mask.size());		
		left_t.resize(mask.size());		
		right_t.resize(mask.size());		

		// multi box
		Ray rays[4];	
		int indices[4];
		int counter = 0;		

		//used in fourRayIntersectBox()
		float left_check[4];
		float temp_left_t[4];
		float right_check[4];
		float temp_right_t[4];
		unsigned int left_counter = 0;
		unsigned int right_counter = 0;
		float left_dist = 0;
		float right_dist = 0;
		for(unsigned int i = 0; i < mask.size();i++)
		{
			left_mask[i] = false; // init to false
			right_mask[i] = false; // init to false
			left_t[i] = -1; 
			right_t[i] = -1;
			if(!mask[i]) continue;
			
			rays[counter] = rayPacket.rayInfo[i];
			indices[counter] = i;
			counter++;

			//do intersect check when we find 4 boxes
			if(counter == 4)
			{				
				((AABB*)objects->at(0))->fourRayIntersectBox(rays, temp_left_t, left_check);
				((AABB*)objects->at(1))->fourRayIntersectBox(rays, temp_right_t, right_check);

				for(int j = 0; j < 4; j++)
				{
					if(left_check[j] != 0) //hits left
					{				
						left_counter++;
						left_mask[indices[j]] = true;
						left_t[indices[j]] = temp_left_t[j];
						left_dist += temp_left_t[j];
					}						
					if(right_check[j] != 0) //hits right
					{						
						right_counter++;
						right_mask[indices[j]] = true;
						right_t[indices[j]] = temp_right_t[indices[j]];
						right_dist += temp_right_t[indices[j]];
					}						
				}
				counter = 0;
			}					
		}
		//there may be rays unchecked, so we do this check
		if(counter != 0)
		{
			((AABB*)objects->at(0))->fourRayIntersectBox(rays, temp_left_t, left_check);
			((AABB*)objects->at(1))->fourRayIntersectBox(rays, temp_right_t, right_check);			
			while(counter > 0)
			{
				if(left_check[counter] != 0 ) //hits left
				{
					left_counter++;
					left_mask[indices[counter]] = true;
					left_t[indices[counter]] = temp_left_t[counter];		
					left_dist += temp_left_t[counter];
				}						
				if(right_check[counter] != 0) //hits right
				{
					right_counter++;
					right_mask[indices[counter]] = true;
					right_t[indices[counter]] = temp_right_t[counter];
					right_dist += temp_right_t[counter];
				}		
				counter--;
			}			
		}
		if(right_counter + left_counter == 0){
			//std::cout<<"nohits\n";
			return; // return if there where no hits
		}
		
		//average the distances of left and right
		left_dist /= (float)left_counter;
		right_dist /= (float)right_counter;

		// intersect the closer one first
		if(left_dist < right_dist){
			((AABB*)objects->at(0))->packetIntersect(rayPacket, left_mask, tMin, tMax);
			for(int i = 0; i < left_mask.size(); i++)
			{
				if(rayPacket.hitMask[i])
				{
					if(rayPacket.hitInfo[i].t < right_t[i])
					{
						right_mask[i] = false;
					}
				}			
			}
			((AABB*)objects->at(1))->packetIntersect(rayPacket, right_mask, tMin, tMax);
		}else{

			((AABB*)objects->at(1))->packetIntersect(rayPacket, right_mask, tMin, tMax);
			for(int i = 0; i < right_mask.size(); i++)
			{
				if(rayPacket.hitMask[i])
				{
					if(rayPacket.hitInfo[i].t < left_t[i])
					{
						left_mask[i] = false;
					}
				}			
			}
			((AABB*)objects->at(0))->packetIntersect(rayPacket, left_mask, tMin, tMax);	
		}
	}
	else
	{		
		///// if this box is a leaf, then all children are triangles/spheres
		for(unsigned int i = 0; i < mask.size(); i++)
		{
			if(mask[i])
			{								
				HitInfo info;
				for(unsigned int j = 0; j < objects->size(); ++j)
				{	
					if(objects->at(j)->intersect( info, rayPacket.rayInfo[i], tMin, tMax))
					{
						if(info.t < rayPacket.hitInfo[i].t || rayPacket.hitMask[i] == false)
						{
							rayPacket.hitInfo[i] = info;
							rayPacket.hitMask[i] = true;
						}
					}
				}
			}
		}		
	}
	
	return;
}


// fills the check array with either 
//		all "0" if NO_HIT
//			OR
//		all "1" if HIT
static __m128 zeros = _mm_set_ps1(0);
void
AABB::fourRayIntersectBox(const Ray ray[4], float t[4], float check[4])
{	
	/* // FAIL this is why u should check the order of the values
	__m128 pos_x = _mm_set_ps(ray[0].o.x,ray[1].o.x,ray[2].o.x,ray[3].o.x);
	__m128 pos_y = _mm_set_ps(ray[0].o.y,ray[1].o.y,ray[2].o.y,ray[3].o.y);
	__m128 pos_z = _mm_set_ps(ray[0].o.z,ray[1].o.z,ray[2].o.z,ray[3].o.z);

	__m128 dir_x = _mm_set_ps(ray[0].d.x,ray[1].d.x,ray[2].d.x,ray[3].d.x);
	__m128 dir_y = _mm_set_ps(ray[0].d.y,ray[1].d.y,ray[2].d.y,ray[3].d.y);
	__m128 dir_z = _mm_set_ps(ray[0].d.z,ray[1].d.z,ray[2].d.z,ray[3].d.z);
	*/
	num_ray_box++;

	__m128 pos_x = _mm_set_ps(ray[3].o.x,ray[2].o.x,ray[1].o.x,ray[0].o.x);
	__m128 pos_y = _mm_set_ps(ray[3].o.y,ray[2].o.y,ray[1].o.y,ray[0].o.y);
	__m128 pos_z = _mm_set_ps(ray[3].o.z,ray[2].o.z,ray[1].o.z,ray[0].o.z);

	__m128 dir_x = _mm_set_ps(ray[3].d.x,ray[2].d.x,ray[1].d.x,ray[0].d.x);
	__m128 dir_y = _mm_set_ps(ray[3].d.y,ray[2].d.y,ray[1].d.y,ray[0].d.y);
	__m128 dir_z = _mm_set_ps(ray[3].d.z,ray[2].d.z,ray[1].d.z,ray[0].d.z);
	
	__m128 min_x = _mm_set_ps1(a.x);
	__m128 min_y = _mm_set_ps1(a.y);
	__m128 min_z = _mm_set_ps1(a.z);

	__m128 max_x = _mm_set_ps1(b.x);
	__m128 max_y = _mm_set_ps1(b.y);
	__m128 max_z = _mm_set_ps1(b.z);


	// get maxmimum min value
	// get minimum max value

	//t1 = (a-ray.o)/ray.d;	
	__m128 t1_x = _mm_div_ps(_mm_sub_ps(min_x, pos_x),dir_x);
	__m128 t1_y = _mm_div_ps(_mm_sub_ps(min_y, pos_y),dir_y);
	__m128 t1_z = _mm_div_ps(_mm_sub_ps(min_z, pos_z),dir_z);

	__m128 t2_x = _mm_div_ps(_mm_sub_ps(max_x, pos_x),dir_x);
	__m128 t2_y = _mm_div_ps(_mm_sub_ps(max_y, pos_y),dir_y);
	__m128 t2_z = _mm_div_ps(_mm_sub_ps(max_z, pos_z),dir_z);
	/*
	tempMin_x = _mm_min_ps(t1_x,t2_x);
	tempMin_y = _mm_min_ps(t1_y,t2_y);
	tempMin_z = _mm_min_ps(t1_z,t2_z);

	tempMax_x = _mm_max_ps(t1_x,t2_x);
	tempMax_y = _mm_max_ps(t1_y,t2_y);
	tempMax_z = _mm_max_ps(t1_z,t2_z);

	min_x = _mm_max_ps(_mm_max_ps(tempMin_x, tempMin_y), tempMin_z);
	max_x = _mm_min_ps(_mm_min_ps(tempMax_x, tempMax_y), tempMax_z);
	*/
	min_x = _mm_max_ps(_mm_min_ps(t1_x,t2_x), _mm_min_ps(t1_y,t2_y));
	min_x = _mm_max_ps(min_x, _mm_min_ps(t1_z,t2_z) );
	max_x = _mm_min_ps(_mm_max_ps(t1_x,t2_x), _mm_max_ps(t1_y,t2_y) );
	max_x = _mm_min_ps(max_x, _mm_max_ps(t1_z,t2_z));
	
	//if(tmax >= 0 && tmax >= tmin)
	_mm_storeu_ps(check, _mm_and_ps(_mm_cmpge_ps(max_x, zeros),_mm_cmpge_ps(max_x, min_x ) ) );
	
	_mm_storeu_ps(t, min_x);
	/*
	__m128 m_check = _mm_cmple_ps(min_x, zeros);
	min_x = _mm_and_ps(m_check, min_x);
	max_x = _mm_andnot_ps(m_check, max_x);
	_mm_storeu_ps(t, _mm_and_ps( min_x, max_x));
	*/
	//_mm_storeu_ps(t, _mm_and_ps(_mm_and_ps(_mm_cmpge_ps(max_x, zeros),_mm_cmpge_ps(max_x, min_x ) ), min_x ));
	//_mm_storeu_ps(check, _mm_cmpge_ps(max_x, min_x) );

	//std::cout<<t[0] << " " <<  t[1] << " " << t[2] << " " << t[3] << std::endl;
}


bool
AABB::intersectBox(const Ray& ray, float& t)
{
	num_ray_box++;

	//single ray single box intersect
	__m128 rayPos = _mm_set_ps(0, ray.o.z, ray.o.y, ray.o.x);
	__m128 rayDir = _mm_set_ps(1, ray.d.z, ray.d.y, ray.d.x);
	__m128 t1 = _mm_set_ps(0, a.z, a.y, a.x);
	t1 = _mm_sub_ps(t1, rayPos);
	t1 = _mm_div_ps(t1, rayDir);

	__m128 t2 = _mm_set_ps(0, b.z, b.y, b.x);
	t2 = _mm_sub_ps(t2, rayPos);
	t2 = _mm_div_ps(t2, rayDir);
	__m128 min = _mm_min_ps(t1, t2);  
	__m128 max = _mm_max_ps(t1, t2);

	// xyzx: 0,1,2,0  : 00 01 10 00 : 1 8
	// yzxy: 1,2,0,1  : 01 10 00 01 : 6 1
	// zxyz: 2,0,1,2  : 10 00 01 10 : 8 6	
	//std::cout<< min.m128_f32[0] << " " << min.m128_f32[1] << " " << min.m128_f32[2] << " " << min.m128_f32[3] << std::endl;
	//std::cout<< max.m128_f32[0] << " " << max.m128_f32[1] << " " << max.m128_f32[2] << " " << max.m128_f32[3] << std::endl;

	min = _mm_max_ps( _mm_max_ps(	_mm_shuffle_ps(min, min, 0x1F),
									_mm_shuffle_ps(min, min, 0x6F)), 
									_mm_shuffle_ps(min, min, 0x8F)); 
	
	max = _mm_min_ps( _mm_min_ps(	_mm_shuffle_ps(max, max, 0x18),
									_mm_shuffle_ps(max, max, 0x61)), 
									_mm_shuffle_ps(max, max, 0x86)); 

	__m128 check = _mm_cmplt_ps(max, min);
	
	if(check.m128_f32[2] || check.m128_f32[0]) // if(tmax < 0 || tmin > tmax)
		return false;
	t = min.m128_f32[2];	
	
	return true;

	/*
	Vector3 t1;
	Vector3 t2;
	float tmin;
	float tmax;
	t1.x = (a.x-ray.o.x)/ray.d.x;
	t2.x = (b.x-ray.o.x)/ray.d.x;
	t1.y = (a.y-ray.o.y)/ray.d.y;
	t2.y = (b.y-ray.o.y)/ray.d.y;
	t1.z = (a.z-ray.o.z)/ray.d.z;
	t2.z = (b.z-ray.o.z)/ray.d.z;
	tmin = max( min(t1.x, t2.x), min(t1.y,t2.y));
	tmin = max( tmin, min(t1.z,t2.z));

	tmax = min( max(t1.x, t2.x), max(t1.y,t2.y));
	tmax = min( tmax, max(t1.z,t2.z));
	if(tmax < 0)
		return false;
	if(tmin > tmax) /// equal sign ???
		return false;

	 return true;

	 */	

}

void AABB::resetCounters()
{
	num_ray_box= 0;
}