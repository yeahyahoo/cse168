#include <math.h>
#include "Miro.h"
#include "Scene.h"
#include "Camera.h"
#include "Image.h"
#include "Console.h"

#include "PointLight.h"
#include "Sphere.h"
#include "TriangleMesh.h"
#include "Triangle.h"
#include "Lambert.h"
#include "Reflective.h"
#include "Refractive.h"
#include "MiroWindow.h"
#include "StoneTexture.h"

#include "assignment1.h"
#include "assignment2.h"

void
makeSpiralScene()
{
    g_camera = new Camera;
    g_scene = new Scene;
    g_image = new Image;

    g_image->resize(512, 512);
    
    // set up the camera
    g_camera->setBGColor(Vector3(1.0f, 1.0f, 1.0f));
    g_camera->setEye(Vector3(-5, 2, 3));
    g_camera->setLookAt(Vector3(0, 0, 0));

    // create and place a point light source
    PointLight * light = new PointLight;
    light->setPosition(Vector3(-3, 15, 3));
    light->setColor(Vector3(1, 1, 1));
    g_camera->setUp(Vector3(0, 1, 0));
    g_camera->setFOV(45);
    light->setWattage(1000);
    g_scene->addLight(light);

    // create a spiral of spheres
    Material* diffuse = new Lambert(Vector3(1.0f, 0.0f, 0.0f));
	Material* specuar = new Refractive(	Vector3(0.3),
									Vector3(0.05),
									Vector3(0.7),
									16,
									1.3);
    const int maxI = 200;
    const float a = 0.15f;
    for (int i = 1; i < maxI; ++i)
    {
        float t = i/float(maxI);
        float theta = 4*PI*t;
        float r = a*theta;
        float x = r*cos(theta);
        float y = r*sin(theta);
        float z = 2*(2*PI*a - r);
        Sphere * sphere = new Sphere;
        sphere->setCenter(Vector3(x,y,z));
        sphere->setRadius(r/10);
        sphere->setMaterial(specuar);
        g_scene->addObject(sphere);
    }
    
    // let objects do pre-calculations if needed
    g_scene->preCalc();
}



void
makeLorentzScene()
{
    g_camera = new Camera;
    g_scene = new Scene;
    g_image = new Image;

    g_image->resize(512, 512);
    
    // set up the camera
    g_camera->setBGColor(Vector3(1.0f, 1.0f, 1.0f));
    g_camera->setEye(Vector3(-5, 2, 3));
    g_camera->setLookAt(Vector3(0, 0, 0));
    g_camera->setUp(Vector3(0, 1, 0));
    g_camera->setFOV(45);

    // create and place a point light source
    PointLight * light = new PointLight;
    light->setPosition(Vector3(-3, 15, 3));
    light->setColor(Vector3(1, 1, 1));
    light->setWattage(10000);
    g_scene->addLight(light);

    // create a spiral of spheres
    Material* mat = new Lambert(Vector3(1.0f, 0.0f, 0.0f));
	Material* ice = new Refractive(	Vector3(0.3),
										Vector3(0.05),
										Vector3(0.9),
										16,
										1.31);
    const int maxI = 1000;
    const float a = 0.15f;

	const float sigma = 10;
	const float beta = 8.0/3.0;
	const float pi = 28;
	
	float x,y,z;
	float dx,dy,dz;

	x = y = z = 1;

	float dt = 24.0/(float)maxI;

    for (int i = 1; i < maxI; ++i)
    {
        float t = i/float(maxI);
        float theta = 4*PI*t;
        float r = a*theta;
		
		dx = sigma*(y-x) ;
		dy = x*(pi-z)-y ;
		dz = x*y-beta*z;
				
		x += dx * dt;
        y += dy * dt; 
        z += dz * dt;

        Sphere * sphere = new Sphere;
        sphere->setCenter(Vector3(x/10,y/10,z/10));
        sphere->setRadius(r/10);
        sphere->setMaterial(ice);
        g_scene->addObject(sphere);

    }
    
    // let objects do pre-calculations if needed
    g_scene->preCalc();
}

void makeTest()
{
	g_camera = new Camera;
    g_scene = new Scene;
    g_image = new Image;

    g_image->resize(512, 512);
    
    // set up the camera
    g_camera->setBGColor(Vector3(1.0f, 1.0f, 1.0f));
    g_camera->setEye(Vector3(-5, 2, 0));
    g_camera->setLookAt(Vector3(0, 1, 0));
    g_camera->setUp(Vector3(0, 1, 0));
    g_camera->setFOV(45);

    // create and place a point light source
    PointLight * light = new PointLight;
    light->setPosition(Vector3(-3, 15, 3));
    light->setColor(Vector3(1, 1, 1));
    light->setWattage(1000);
    g_scene->addLight(light);

	light = new PointLight;
    light->setPosition(Vector3(3, 15, -3));
    light->setColor(Vector3(1, 1, 1));
    light->setWattage(1000);
    g_scene->addLight(light);

    // create a spiral of spheres
    Material* white = new Lambert(Vector3(1.0f, 1.0f, 1.0f));
	Material* black = new Lambert(Vector3(0.1f, 0.1f, 0.1f));
	Material* red =  new Reflective(Vector3(1.0f, 0.0f, 0.0f), 
									Vector3(0.05),
									Vector3(.7));
	Material* blue = new Reflective(Vector3(0.0f, 0.0f, 1.0f),
									Vector3(.05),
									Vector3(.7));

	Material* refract = new Refractive(	Vector3(0.3),
										Vector3(0.05),
										Vector3(0.9),
										16,
										1.31); // ice

	Material* stone = new StoneTexture();

	Sphere * sphere = new Sphere;
    sphere->setCenter(Vector3(0,1,0));
    sphere->setRadius(1);
    sphere->setMaterial(stone);
	g_scene->addObject(sphere);


    // create the floor triangle
    TriangleMesh * floor = new TriangleMesh;
    floor->createSingleTriangle();
    floor->setV1(Vector3( 10, 0,  10));
    floor->setV2(Vector3( 10, 0, -10));
    floor->setV3(Vector3(-10, 0, -10));
    floor->setN1(Vector3(0, 1, 0));
    floor->setN2(Vector3(0, 1, 0));
    floor->setN3(Vector3(0, 1, 0));
    
    Triangle* t = new Triangle;
    t->setIndex(0);
    t->setMesh(floor);
    t->setMaterial(red); 
    g_scene->addObject(t);

	floor = new TriangleMesh;
    floor->createSingleTriangle();
    floor->setV1(Vector3(-10, 0, -10));
    floor->setV2(Vector3(-10, 0,  10));
    floor->setV3(Vector3( 10, 0,  10));
    floor->setN1(Vector3(0, 1, 0));
    floor->setN2(Vector3(0, 1, 0));
    floor->setN3(Vector3(0, 1, 0));
    
    t = new Triangle;
    t->setIndex(0);
    t->setMesh(floor);
    t->setMaterial(blue); 
    g_scene->addObject(t);

    // let objects do pre-calculations if needed
    g_scene->preCalc();
}


void makeSphereTest()
{
	g_camera = new Camera;
    g_scene = new Scene;
    g_image = new Image;

    g_image->resize(512, 512);
    
    // set up the camera
    g_camera->setBGColor(Vector3(1.0f, 1.0f, 1.0f));
    g_camera->setEye(Vector3(-5, 2, 0));
    g_camera->setLookAt(Vector3(0, 1, 0));
    g_camera->setUp(Vector3(0, 1, 0));
    g_camera->setFOV(45);

    // create and place a point light source
    PointLight * light = new PointLight;
    light->setPosition(Vector3(-3, 15, 3));
    light->setColor(Vector3(1, 1, 1));
    light->setWattage(1000);
    g_scene->addLight(light);

	light = new PointLight;
    light->setPosition(Vector3(3, 15, -3));
    light->setColor(Vector3(1, 1, 1));
    light->setWattage(1000);
    g_scene->addLight(light);

    // create a spiral of spheres
    Material* white = new Lambert(Vector3(1.0f, 1.0f, 1.0f));
	Material* black = new Lambert(Vector3(0.1f, 0.1f, 0.1f));
	Material* shiny = new Reflective(	Vector3(0.0f, 0.0f, 0.0f), 
										Vector3(0.05),
										Vector3(.8));
	Material* red =  new Reflective(Vector3(1.0f, 0.0f, 0.0f), 
									Vector3(0.05),
									Vector3(.7));
	Material* blue = new Reflective(Vector3(0.0f, 0.0f, 1.0f),
									Vector3(.05),
									Vector3(.7));

	Material* refract = new Refractive(	Vector3(0.3),
										Vector3(0.05),
										Vector3(0.9),
										16,
										1.30); // ice

	Sphere * sphere = new Sphere;
    sphere->setCenter(Vector3(0,1,2));
    sphere->setRadius(1);
    sphere->setMaterial(refract);
	g_scene->addObject(sphere);

	sphere = new Sphere;
    sphere->setCenter(Vector3(0,1,0));
    sphere->setRadius(1);
    sphere->setMaterial(white);
	g_scene->addObject(sphere);

	sphere = new Sphere;
    sphere->setCenter(Vector3(0,1,-2));
    sphere->setRadius(1);
    sphere->setMaterial(shiny);
	g_scene->addObject(sphere);


    // create the floor triangle
    TriangleMesh * floor = new TriangleMesh;
    floor->createSingleTriangle();
    floor->setV1(Vector3( 10, 0,  10));
    floor->setV2(Vector3( 10, 0, -10));
    floor->setV3(Vector3(-10, 0, -10));
    floor->setN1(Vector3(0, 1, 0));
    floor->setN2(Vector3(0, 1, 0));
    floor->setN3(Vector3(0, 1, 0));
    
    Triangle* t = new Triangle;
    t->setIndex(0);
    t->setMesh(floor);
    t->setMaterial(red); 
    g_scene->addObject(t);

	floor = new TriangleMesh;
    floor->createSingleTriangle();
    floor->setV1(Vector3(-10, 0, -10));
    floor->setV2(Vector3(-10, 0,  10));
    floor->setV3(Vector3( 10, 0,  10));
    floor->setN1(Vector3(0, 1, 0));
    floor->setN2(Vector3(0, 1, 0));
    floor->setN3(Vector3(0, 1, 0));
    
    t = new Triangle;
    t->setIndex(0);
    t->setMesh(floor);
    t->setMaterial(blue); 
    g_scene->addObject(t);

    // let objects do pre-calculations if needed
    g_scene->preCalc();
}

int
main(int argc, char*argv[])
{
    // create a scene
    //makeSpiralScene();
	//makeLorentzScene();
	//makeBunnyScene();
	makeCornellScene();
	//makeSphereTest();
	//makeTest();
	//makeSphereScene();
	
	//makeTeapotScene();
	//makeBunny1Scene();
	//makeBunny20Scene();
	//makeSponzaScene();
	//makeCornellScene2();

	//cornerTeapotScene();

    MiroWindow miro(&argc, argv);
    miro.mainLoop();

    return 0; // never executed
}

