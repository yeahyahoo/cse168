#ifndef _AREALIGHT_H_
#define _AREALIGHT_H_

#include "Light.h"
#include "Ray.h"
#include "Vector3.h"
#include <vector>
#include <math.h>

class AreaLight : public Light
{
public:
    void setU(const Vector3& v)     {m_u = v;}
    void setV(const Vector3& v)     {m_v = v;}
        
	virtual void getStratifiedSamples(Ray* ray, int& u_samples, int& v_samples)//TODO
	{		
		Vector3 u_axis = m_u.normalized();
		Vector3 v_axis = m_v.normalized();
		float theta, phi;
		float x,y,z;

		float u_interval = 1.0f/u_samples;
		float v_interval = 1.0f/v_samples;		
		for(int i = 0; i < u_samples; i++)
		{
			for(int j = 0; j < v_samples; j++)
			{
				Vector3 a = m_u*(1-2*(i/u_samples + u_interval*rand()/(float)RAND_MAX));
				Vector3 b = m_v*(1-2*(j/v_samples + v_interval*rand()/(float)RAND_MAX));				

				theta = PI/2*rand()/(float)RAND_MAX;
				phi = 2*PI*rand()/(float)RAND_MAX;

				x = sin(theta)*cos(phi);	
				y =	sin(theta)*sin(phi);	
				z = cos(theta);	
				
				ray[i*u_samples + j].o = a+b+m_position;
				ray[i*u_samples + j].d = Vector3(x,y,z);
			}
		}
	}
	virtual void getSample(Ray& ray) //TODO
	{
		Vector3 u_axis = m_u.normalized();
		Vector3 v_axis = m_v.normalized();
		float theta, phi;

		Vector3 x = m_u*(1-2*rand()/(float)RAND_MAX);
		Vector3 y = m_v*(1-2*rand()/(float)RAND_MAX);

		
		theta = PI*rand()/(float)RAND_MAX;
		phi = 2*PI*rand()/(float)RAND_MAX;

		ray.o = x+y+m_position;
		ray.d = (ray.o).normalized();
	}
	virtual void getStratifiedShadowRay(const Vector3 hitpos, Ray* ray, int& u_samples, int& v_samples)
	{
		float u_interval = 1.0f/u_samples;
		float v_interval = 1.0f/v_samples;		
		for(int i = 0; i < u_samples; i++)
		{
			for(int j = 0; j < v_samples; j++)
			{
				Vector3 x = m_u*(1-2*(i/u_samples + u_interval*rand()/(float)RAND_MAX));
				Vector3 y = m_v*(1-2*(j/v_samples + v_interval*rand()/(float)RAND_MAX));				
				ray[i*u_samples + j].o = x+y+m_position;
				ray[i*u_samples + j].d = (hitpos-ray[i*u_samples + j].o).normalized();
			}
		}
	}
	virtual void getShadowRay(const Vector3 hitpos, Ray& ray)
	{
		Vector3 x = m_u*(1-2*rand()/(float)RAND_MAX);
		Vector3 y = m_v*(1-2*rand()/(float)RAND_MAX);
		ray.o = x+y+m_position;
		ray.d = (hitpos-ray.o).normalized();
	}
	virtual Vector3 positionShadow(Vector3 hitpos) 
	{
		Vector3 x = m_u*(1-2*rand()/(float)RAND_MAX);
		Vector3 y = m_v*(1-2*rand()/(float)RAND_MAX);
		return x+y+m_position;
	}
protected:
    Vector3 m_u;
    Vector3 m_v;
};

#endif 
