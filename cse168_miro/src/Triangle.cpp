#include "Triangle.h"
#include "TriangleMesh.h"
#include "Ray.h"

#include <smmintrin.h>

long unsigned int Triangle::num_ray_tri = 0;
Triangle::Triangle(TriangleMesh * m, unsigned int i) :
    m_mesh(m), m_index(i)
{

}


Triangle::~Triangle()
{

}

void 
Triangle::getAABB(Vector3& min, Vector3& max)
{
	TriangleMesh::TupleI3 ti3 = m_mesh->vIndices()[m_index];
    const Vector3 & v0 = m_mesh->vertices()[ti3.x]; //vertex a of triangle
    const Vector3 & v1 = m_mesh->vertices()[ti3.y]; //vertex b of triangle
    const Vector3 & v2 = m_mesh->vertices()[ti3.z]; //vertex c of triangle

	min.x = std::min(std::min(v0.x, v1.x),v2.x);
	min.y = std::min(std::min(v0.y, v1.y),v2.y);
	min.z = std::min(std::min(v0.z, v1.z),v2.z);

	max.x = std::max(std::max(v0.x, v1.x),v2.x);
	max.y = std::max(std::max(v0.y, v1.y),v2.y);
	max.z = std::max(std::max(v0.z, v1.z),v2.z);
}

void
Triangle::renderGL()
{
    TriangleMesh::TupleI3 ti3 = m_mesh->vIndices()[m_index];
    const Vector3 & v0 = m_mesh->vertices()[ti3.x]; //vertex a of triangle
    const Vector3 & v1 = m_mesh->vertices()[ti3.y]; //vertex b of triangle
    const Vector3 & v2 = m_mesh->vertices()[ti3.z]; //vertex c of triangle

    glBegin(GL_TRIANGLES);
        glVertex3f(v0.x, v0.y, v0.z);
        glVertex3f(v1.x, v1.y, v1.z);
        glVertex3f(v2.x, v2.y, v2.z);
    glEnd();
}



bool
Triangle::intersect2(HitInfo& result, const Ray& r,float tMin, float tMax)
{	
	num_ray_tri++;
	Vector3 a,b,c, ab, ac, n, w;
	float nDotRay, nDotEye;
	float alpha, beta, gamma;

	TriangleMesh::TupleI3 ti3 = m_mesh->vIndices()[m_index];
	a = m_mesh->vertices()[ti3.x];
	b = m_mesh->vertices()[ti3.y];
	c = m_mesh->vertices()[ti3.z];

	ab = b - a;
	ac = c - a;	
	n = cross(ab, ac);

	nDotRay = dot(n, -r.d);
		
	if( nDotRay ==  0) return false; // check for eye ray parallel to plane

	w = r.o - a;
	nDotEye = dot(w, n);

	result.t = nDotEye/nDotRay; // get the distance to the hitpoint from the eye

	if(result.t < tMin || result.t > tMax) return false;
    
	//Get the intersection with the PLANE, not yet know if in triangle    
	result.P = r.o + result.t * r.d;   
	
	// first calculate barycentric coordinates
	alpha = dot( -r.d, cross( w, ac))/nDotRay;
	beta  = dot( -r.d, cross(ab,  w))/nDotRay;

	// triangle intersection test
	if(alpha + beta > 1.0 || alpha < 0.0 || beta < 0.0) return false;
	gamma = 1.0 - alpha - beta;

	ti3 = m_mesh->nIndices()[m_index];

	//interpolate normal	
	n = gamma*m_mesh->normals()[ti3.x] + alpha*m_mesh->normals()[ti3.y] + beta*m_mesh->normals()[ti3.z];

	result.material = m_material;
	result.N = n.normalized();	//TODO: get this removed
		
    return true;
}

__declspec(align(16)) struct Aligned_16{
    union
    {
        struct { float x, y, z, pad; };
        __m128 mmvalue;
    };    
    Aligned_16(void):mmvalue(_mm_setzero_ps())
    {        
    }
    Aligned_16(Vector3 v):mmvalue(_mm_set_ps(0, v.z, v.y, v.x)) 
    {        
    }
    Aligned_16(float a, float b, float c, float d):mmvalue(_mm_set_ps(d, c, b, a)){        
    };
	Aligned_16(float a):mmvalue(_mm_set_ps(a, a, a, a)){        
    };
    Aligned_16(__m128 _derp): mmvalue(_derp)
    {
    }
    Vector3 getVec(){
        return Vector3(x,y,z);
    };
};

__m128 vector_to_m128(Vector3 v)
{
	return _mm_set_ps(0, v.z, v.y, v.x);
}


static __m128 ones = _mm_set_ps(1,1,1,1);
static __m128 zeros = _mm_set_ps(0,0,0,0);
bool
Triangle::intersect(HitInfo& result, const Ray& r,float tMin, float tMax)
{
	num_ray_tri++;
	Vector3 a,b,c;
	TriangleMesh::TupleI3 ti3 = m_mesh->vIndices()[m_index];
	a = m_mesh->vertices()[ti3.x];
	b = m_mesh->vertices()[ti3.y];
	c = m_mesh->vertices()[ti3.z];

	Aligned_16 convert;

	__m128 vertex_A = vector_to_m128(a);
    __m128 vertex_B = vector_to_m128(b);
    __m128 vertex_C = vector_to_m128(c);
    __m128 ray_o = vector_to_m128(r.o);
    __m128 ray_d = vector_to_m128(-r.d);

	//ab = b - a;
	//ac = c - a;	
	__m128 line_AB = _mm_sub_ps(vertex_B, vertex_A);
    __m128 line_AC = _mm_sub_ps(vertex_C, vertex_A);

	//n = cross(ab, ac); // how to crossproduct
	// 0xD2 = 1101 0010
	// 0xC9 = 1100 1001
	__m128 normal = _mm_sub_ps( _mm_mul_ps(_mm_shuffle_ps(line_AB, line_AB, 0xC9), _mm_shuffle_ps(line_AC, line_AC, 0xD2)) ,  
								_mm_mul_ps(_mm_shuffle_ps(line_AB, line_AB, 0xD2), _mm_shuffle_ps(line_AC, line_AC, 0xC9)) );
    

	//nDotRay = dot(n, -r.d);
	__m128 nDotRay = _mm_dp_ps(normal, ray_d, 0x7F); // bit mask = 0111 1111	

	//if( abs(nDotRay) < epsilon) return false; // check for eye ray parallel to plane	
	if( nDotRay.m128_f32[0] == 0) return false; // check for eye ray parallel to plane	
	
	//w = r.o - a;
	const __m128 w = _mm_sub_ps(ray_o, vertex_A);
	//nDotEye = dot(w, n);
	__m128 nDotEye = _mm_dp_ps(w, normal, 0xFF); // bit mask = 0111 1111	

	//result.t = nDotEye/ nDotRay; // get the distance to the hitpoint from the eye
	__m128 t = _mm_div_ps(nDotEye, nDotRay);	

	//if(result.t < tMin || result.t > tMax) return false;	
	if(t.m128_f32[0] < tMin || t.m128_f32[0] > tMax) return false;
	
	//Get the intersection with the PLANE, not yet know if in triangle    
	//result.P = r.o + result.t * r.d;   
	__m128 hitPoint = _mm_add_ps(ray_o, _mm_mul_ps(t, vector_to_m128(r.d)));	
	
	// first calculate barycentric coordinates
	//alpha = dot( -r.d, cross( w, ac))/nDotRay;
	__m128 alpha = _mm_sub_ps(	_mm_mul_ps(_mm_shuffle_ps(w, w, 0xC9), _mm_shuffle_ps(line_AC, line_AC, 0xD2)) ,  
								_mm_mul_ps(_mm_shuffle_ps(w, w, 0xD2), _mm_shuffle_ps(line_AC, line_AC, 0xC9)) );	
	alpha = _mm_div_ps(_mm_dp_ps(ray_d, alpha, 0xFF), nDotRay);

	//beta  = dot( -r.d, cross(ab,  w))/nDotRay;
	__m128 beta = _mm_sub_ps(	_mm_mul_ps(_mm_shuffle_ps(line_AB, line_AB, 0xC9), _mm_shuffle_ps(w, w, 0xD2)) ,  
								_mm_mul_ps(_mm_shuffle_ps(line_AB, line_AB, 0xD2), _mm_shuffle_ps(w, w, 0xC9)) );
	beta = _mm_div_ps(_mm_dp_ps(ray_d, beta, 0xFF), nDotRay);
	
	//gamma = 1.0 - alpha - beta;	
	__m128 gamma = _mm_sub_ps(_mm_sub_ps(ones, alpha), beta);	


	// triangle intersection test
	//if(alpha + beta > 1 || alpha < 0.0 || beta < 0.0) return false;
	if(	alpha.m128_f32[0] + beta.m128_f32[0] > 1.0 || 
		alpha.m128_f32[0] < 0.0 || 
		beta.m128_f32[0] < 0.0 ) return false;


	ti3 = m_mesh->nIndices()[m_index];

	Aligned_16 normal_a(m_mesh->normals()[ti3.x]);
	Aligned_16 normal_b(m_mesh->normals()[ti3.y]);
	Aligned_16 normal_c(m_mesh->normals()[ti3.z]);

	//interpolate normal
	//n = gamma*m_mesh->normals()[ti3.x] + alpha*m_mesh->normals()[ti3.y] + beta*m_mesh->normals()[ti3.z];
	gamma = _mm_mul_ps(gamma, vector_to_m128(m_mesh->normals()[ti3.x]));
	alpha = _mm_mul_ps(alpha, vector_to_m128(m_mesh->normals()[ti3.y]));
	beta  = _mm_mul_ps(beta, vector_to_m128(m_mesh->normals()[ti3.z]));

	normal = _mm_add_ps(_mm_add_ps(gamma, alpha), beta);	

	result.t = t.m128_f32[0];
	result.material = m_material;
	result.P = Aligned_16(hitPoint).getVec();	
	result.N = Aligned_16(normal).getVec();

	result.N.normalize();// TODO: get this removed
	

    return true;
}

void Triangle::resetCounters()
{
	num_ray_tri = 0;
}