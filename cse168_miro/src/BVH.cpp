#include "BVH.h"
#include "Ray.h"
#include "Console.h"
#include "Triangle.h"
#include "TriangleMesh.h"
#include <algorithm>

using namespace std;

long unsigned int BVH::num_BVHNodes = 0;
long unsigned int BVH::num_BVHLeaves = 0;

bool sortX(Object*a, Object*b)
{
	//return(rand()/RAND_MAX) > .5;
	Vector3 a_min, a_max;
	Vector3 b_min, b_max;
	a->getAABB(a_min, a_max);
	b->getAABB(b_min, b_max);
	a_min = (a_min+a_max)/2;
	b_min = (b_min+b_max)/2;
	return a_min.x < b_min.x;	
}
bool sortY(Object*a, Object*b)
{
	//return(rand()/RAND_MAX) > .5;
	Vector3 a_min, a_max;
	Vector3 b_min, b_max;
	a->getAABB(a_min, a_max);
	b->getAABB(b_min, b_max);
	a_min = (a_min+a_max)/2;
	b_min = (b_min+b_max)/2;
	return a_min.y < b_min.y;	
}
bool sortZ(Object*a, Object*b)
{
	//return(rand()/RAND_MAX) > .5;
	Vector3 a_min, a_max;
	Vector3 b_min, b_max;
	a->getAABB(a_min, a_max);
	b->getAABB(b_min, b_max);
	a_min = (a_min+a_max)/2;
	b_min = (b_min+b_max)/2;
	return a_min.z < b_min.z;	
}

BVH::BVH(){
	useBVH = true;
	trianglesPerBox = 1;
}

void
BVH::build(Objects * objs)
{
	// construct the bounding volume hierarchy
	m_objects = objs;

	rootBox = new AABB(m_objects);	
	subdivide_node(rootBox,m_objects);
}

bool
BVH::intersect(HitInfo& minHit, const Ray& ray, float tMin, float tMax) const
{
	// Here you would need to traverse the BVH to perform ray-intersection
	// acceleration. For now we just intersect every object.

	bool hit = false;
	HitInfo tempMinHit;
	tempMinHit.t = MIRO_TMAX;

	if(useBVH){
		if(rootBox->intersect(tempMinHit, ray, tMin, tMax))
		{
				hit = true;
				minHit = tempMinHit;
		}

	}else{
		for (size_t i = 0; i < m_objects->size(); ++i)
		{
			if ((*m_objects)[i]->intersect(tempMinHit, ray, tMin, tMax))
			{
				hit = true;
				if (tempMinHit.t < minHit.t)
					minHit = tempMinHit;
			}
		}
	}

	return hit;
}
void
BVH::packetIntersect( RayPacket& rayPacket, float tMin, float tMax) const
{
	/*
	std::vector<bool> mask;
	mask.resize(rayPacket.hitInfo.size());
	for(unsigned int i = 0; i < rayPacket.hitInfo.size(); i++)
	{
		mask[i] = true;
	}
	return rootBox->packetIntersect(rayPacket, mask, tMin, tMax);
	*/	
	
	rootBox->packetIntersect(rayPacket.hitInfo, rayPacket.rayInfo, rayPacket.hitMask, tMin, tMax);

}
void splitObjects(AABB* node, Objects* objList, Objects* leftObjects, Objects* rightObjects)
{
	if(false){
		float splitindex = (objList->size()/2.0f);
		for(int i = 0; i < objList->size() ; ++i){
	
			if( i < splitindex ){
				leftObjects->push_back( (*objList)[i]	 );
			}else{
				rightObjects->push_back( (*objList)[i] );
			}
		}	
		return;
	}


	if(objList->size() == 2){
		leftObjects->push_back(objList->at(0));
		rightObjects->push_back(objList->at(1));
		return;
	}
	int bestAxis = 0; // [0,1,2] correpsonds to [x,y,z]
	float bestCost = MIRO_TMAX;	

	Objects tempObjects; //make a hard copy of the objects
	Vector3 a,b;	
	for(int i = 0; i < objList->size(); ++i){
		tempObjects.push_back(objList->at(i));		
	}
	
	float totalSA = node->getSurfaceArea();
	int totalCount = objList->size();
	int bestLeftCount = 0;
	int bestRightCount = 0;
	int i,j;
	

	Vector3 mean, median, tempMin, tempMax;
	
	// calc the mean position
	for(i = 0; i < objList->size(); ++i){
		objList->at(i)->getAABB(tempMin, tempMax);
		mean = (tempMin+tempMax)/2;
	}
	mean /= (float)objList->size();
	
	
	for(int currAxis = 0; currAxis < 3; ++currAxis)
	{
		Objects tempLeft;
		Objects tempRight;
		switch(currAxis)
		{
		case 0: std::sort(tempObjects.begin(), tempObjects.end(), sortX); break;
		case 1: std::sort(tempObjects.begin(), tempObjects.end(), sortY); break;
		case 2: std::sort(tempObjects.begin(), tempObjects.end(), sortZ); break;
		}

		tempObjects.at(tempObjects.size()/2)->getAABB(tempMin, tempMax);
		median = (tempMin+tempMax)/2;		
	
		for(i = 0; i < tempObjects.size()/2;++i){	
			tempLeft.push_back(tempObjects.at(i));
		}
		for(j = tempObjects.size()-1;j >= i ;--j){
			tempRight.push_back(tempObjects.at(j));
		}
		/*
		AABB leftside(&tempLeft);
		AABB rightside(&tempRight);
		float cost =	(leftside.getSurfaceArea()/totalSA*tempLeft.size())+
						(rightside.getSurfaceArea()/totalSA*tempRight.size());
		if(cost < bestCost)
		{
			//std::cout << "COST CHANGE: "<< cost << " " << bestCost << std::endl;
			bestAxis = currAxis;
			bestCost = cost;
			bestLeftCount = tempLeft.size();
			bestRightCount = tempRight.size();
		}
		*/
		//printf("pre was %i/%i, %i/%i\n", tempLeft.size(),objList->size(), tempRight.size(),objList->size());
		bool leftToRight;
		
		switch(currAxis)
		{
		case 0: leftToRight = median.x < mean.x; break;
		case 1: leftToRight = median.y < mean.y; break;
		case 2: leftToRight = median.z < mean.z; break;
		}
				
		bool notFinished = true;
		while(notFinished)
		{
			AABB leftside(&tempLeft);
			AABB rightside(&tempRight);
			float cost =	(leftside.getSurfaceArea()/totalSA*tempLeft.size())+
							(rightside.getSurfaceArea()/totalSA*tempRight.size());
			if(cost < bestCost)
			{
			//	std::cout << "COST CHANGE: "<< cost << " " << bestCost << std::endl;
				bestAxis = currAxis;
				bestCost = cost;
				bestLeftCount = tempLeft.size();
				bestRightCount = tempRight.size();				
			}
			
			if(leftToRight){ // pop left(less than side), push right(greater than side)							
				tempLeft.back()->getAABB(tempMin, tempMax);				
				
				switch(currAxis)
				{
				case 0: notFinished = mean.x < (tempMin.x+tempMax.x)/2; break;
				case 1: notFinished = mean.y < (tempMin.y+tempMax.y)/2; break;
				case 2: notFinished = mean.z < (tempMin.z+tempMax.z)/2; break;
				}				
				//swap
				tempRight.push_back(tempLeft.back());
				tempLeft.pop_back();
			}else{
				
				tempRight.back()->getAABB(tempMin, tempMax);
				
				switch(currAxis)
				{
				case 0: notFinished = mean.x > (tempMin.x+tempMax.x)/2; break;
				case 1: notFinished = mean.y > (tempMin.y+tempMax.y)/2; break;
				case 2: notFinished = mean.z > (tempMin.z+tempMax.z)/2; break;
				}
				//swap
				tempLeft.push_back(tempRight.back());
				tempRight.pop_back();
			}
			if(tempLeft.size() == 0 || tempRight.size() == 0) notFinished = false;
		}
		
	}
	
	switch(bestAxis)
	{
		case 0: std::sort(tempObjects.begin(), tempObjects.end(), sortX); break;
		case 1: std::sort(tempObjects.begin(), tempObjects.end(), sortY); break;
		case 2: std::sort(tempObjects.begin(), tempObjects.end(), sortZ); break;
	}
	//std::cout << "BestCost: "<< bestCost << std::endl;
	for(i = 0; i < bestLeftCount;++i){
		leftObjects->push_back(tempObjects.at(i));
	}
	for(;i<tempObjects.size();++i){
		rightObjects->push_back(tempObjects.at(i));
	}
	/*
	if(objList->size() > 100)
		printf("split was %i/%i, %i/%i\n", leftObjects->size(),objList->size(), rightObjects->size(),objList->size());	
		*/
	
}

AABB* BVH::subdivide_node(AABB *node, Objects *objList){

	if(node==NULL)
	{
		printf("Node passed in was null\n");
	}

	if(objList->size() == 1){
		node->objects->push_back(objList->front());
		node->isLeaf = true;

		num_BVHLeaves++;
	}else{
		node->isLeaf = false;
		//splittingPoint = getSplittingPoint(current_plane,objList);
		
		Objects leftObjects; ///= new Objects();
		Objects rightObjects ;//= new Objects();


		splitObjects(node,objList, &leftObjects, &rightObjects);

		node->objects->push_back( new AABB(&leftObjects));
		node->objects->push_back( new AABB(&rightObjects));
		
		subdivide_node((AABB*)node->objects->at(0), &leftObjects);		
		subdivide_node((AABB*)node->objects->at(1), &rightObjects);		
		num_BVHNodes++;
	}

	return node;
}

void BVH::drawBoxes(){
	rootBox->renderGL();
}




