#include "Refractive.h"
#include "Ray.h"
#include "Scene.h"

Refractive::Refractive(const Vector3 & kd, const Vector3 & ka, const Vector3 & ks,const float phongexp, const float index_n2, const float index_n1)		
{
	m_kd = kd;
	m_ka = ka;
	m_ks = ks;
	phong_exponent = phongexp;
	m_n2 = index_n2;
	m_n1 = index_n1;
}

Refractive::~Refractive()
{
}

Vector3
Refractive::shade(const Ray& ray, const HitInfo& hit, const Scene& scene) const
{
    HitInfo tempHit;
	tempHit.m_lightIntensity = hit.m_lightIntensity;	

	Ray ray2nd;
	ray2nd.o = hit.P;
    Vector3 L = Vector3(0.0f, 0.0f, 0.0f);
    
	float nDotRay = dot(-ray.d, hit.N);
	Vector3 idealReflect = 2*nDotRay*hit.N + ray.d;		

    const Lights *lightlist = scene.lights();
    
    // loop over all of the lights
    Lights::const_iterator lightIter;
    for (lightIter = lightlist->begin(); lightIter != lightlist->end(); lightIter++)
    {
        Light* pLight = *lightIter;
    
        Vector3 l = pLight->position() - hit.P;
		
		//hard shadow
		pLight->getShadowRay(hit.P, ray2nd);						
		if( scene.trace(tempHit, ray2nd, epsilon)){
			if(l.length2() > (tempHit.P-ray2nd.o).length2())
				continue;
		}
		ray2nd.o = hit.P;

        // the inverse-squared falloff
        float falloff = l.length2();
        
        // normalize the light direction
        l /= sqrt(falloff); // fewer calculations since we already have the length2

        // get the diffuse component
        float nDotL = dot(hit.N, l);		
        Vector3 result = pLight->color();        
		float energy = std::max(0.0f, nDotL * pLight->wattage() / (4.0f * PI * falloff));
        
        L += result * energy * m_kd;

		float phong = dot(idealReflect, l);   
        if(phong > epsilon && nDotRay > 0) // episilon check gets rid of "tangent phong highlight"
		{			
            L += result * (pow(phong,phong_exponent) /( nDotL )) * energy * m_ks;//calc the phong highlight 
		}
    }
   
	Vector3 normal = hit.N;	
	float relativeIndex;
	float cosI, cosT, n1, n2;
	n1 = m_n1;
	n2 = m_n2;                
	
	
	if(nDotRay < 0) //case that the ray is coming out of the surface
	{
		normal = -hit.N;
		nDotRay = dot( -ray.d,  normal );		
		n1 = m_n2;
		n2 = m_n1; // reverse the order of n1 and n2 for calc        		        
		idealReflect = 2*nDotRay*normal + ray.d;				
	}
	relativeIndex = n1 / n2; 
	
	if(nDotRay != 0) // tangent case
	{
		//////////////////////////////////////////
		// calculate the Fresnel coefficients
		// got equations from http://en.wikipedia.org/wiki/Fresnel_equations
		
		Vector3 tempNorm;
	
		cosI = nDotRay; //costI
		cosT = sqrt( 1 - (cosI * cosI) ); // sinI
		cosT = (n1/n2*cosT); 	
		//CHECK FOR NA, may be cause of black sphere
		cosT = 1 - (cosT * cosT);		
		if(cosT >= 0)
		{
		
			cosT = sqrt(cosT); // dont take a squareroot of a neg number, should not happen since we ignore tangent case
	    
			float r_s = (n1*cosI - n2*cosT)/
						(n1*cosI + n2*cosT); // reflected s-polarized light
			float r_p = (n2*cosI - n1*cosT)/
						(n2*cosI + n1*cosT); // reflected p-polarized light    		
			float r = (r_s*r_s + r_p*r_p) * 0.5f;
			//float m_ks_reflect = 2*r/(1+r); // reflect coefficient
			//float m_ks_reflect = sqrt(r);
			float m_ks_reflect = r;
			float m_ks_refract = 1-r;
				
			/*
			float t_s = (2*n1*cosI)/
						(n1*cosI+n2*cosT); // transmitted s-polarized light
			float t_p = (2*n1*cosI)/
						(n1*cosT+n2*cosI); // transmitted p-polarized light				
			float t = (t_s*t_s + t_p*t_p) * 0.5f;
			//float m_ks_refract = (n1*cosI)/(n2*cosT) * sqrt(t); // trandmit/refract coefficient		
			float m_ks_refract = sqrt(t);
			*/
			//////////////////////////////////////////
	
			
			ray2nd.d = idealReflect;//.normalized();		
			//Do reflect
			if( hit.m_lightIntensity > epsilon)
			{
	            if(scene.trace(tempHit, ray2nd, epsilon))
				{
	                tempHit.m_lightIntensity = hit.m_lightIntensity * m_ks_reflect;
					//L += tempHit.material->shade(ray2nd, tempHit, scene) * m_ks * m_ks_reflect;							
					L += tempHit.material->shade(ray2nd, tempHit, scene) * m_ks_reflect;							
				}else
				{            			
					//L += scene.findEnvCoords(ray2nd.d) * m_ks * m_ks_reflect;			
					L += scene.findEnvCoords(ray2nd.d) * m_ks_reflect;			
				}
				tempHit.m_lightIntensity = hit.m_lightIntensity;
			}
	
			//float checkSqrt = 1 - ((relativeIndex * relativeIndex)*(1-nDotRay*nDotRay));
			float checkSqrt = cosT; 
			//Do refract
			if( checkSqrt > 0 )
			{
				//calc direction of of refracted ray
				ray2nd.d = -relativeIndex * (-ray.d - normal*nDotRay) - (sqrt(checkSqrt))*normal;
				ray2nd.d.normalize();				
				if (hit.m_lightIntensity > epsilon)
				{
	                if(scene.trace(tempHit, ray2nd, epsilon))
					{								
	                    tempHit.m_lightIntensity = hit.m_lightIntensity * m_ks_refract;					
						//L += tempHit.material->shade(ray2nd, tempHit, scene) * m_ks * m_ks_refract; // currently taking half reflect and half refract light together				  						
						L += tempHit.material->shade(ray2nd, tempHit, scene) * m_ks_refract; // currently taking half reflect and half refract light together						
					}else{
						//L += scene.findEnvCoords(ray2nd.d) * m_ks * m_ks_refract;					
						L += scene.findEnvCoords(ray2nd.d) * m_ks_refract;					
					}
				}           
				tempHit.m_lightIntensity = hit.m_lightIntensity;
			}
		}
	}
	

    // add the ambient component
    L += m_ka;
    
    return L;
}
