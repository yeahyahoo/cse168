#ifndef _REFRACTIVE_H_
#define _REFRACTIVE_H_

#include "Reflective.h"
#include "Material.h"


/*
refraction indexes
Vacuum		1.00000
Air at STP	1.00029
Ice			1.31
Water		1.33
CrownGlass	1.52 - 1.65
Diamond		2.417
*/

class Refractive : public Reflective
{
public:
    Refractive(	const Vector3 & kd = Vector3(1),
				const Vector3 & ka = Vector3(0),
				const Vector3 & ks = Vector3(0),
				const float phongexp = 16,
				const float index_n2 = 1.00029f,
				const float index_n1 = 1.00029f );
    virtual ~Refractive();
	
	const float n1() {return m_n1;}
	const float n2() {return m_n2;}

    void setN1(const float index) {m_n1 = index;}
	void setN2(const float index) {m_n2 = index;}

    virtual void preCalc() {}
    
    virtual Vector3 shade(const Ray& ray, const HitInfo& hit,
                          const Scene& scene) const;
protected:
    float m_n1;
	float m_n2;
};

#endif // CSE168_LAMBERT_H_INCLUDED
