#ifndef _REFLECTIVE_H_
#define _REFLECTIVE_H_

#include "Lambert.h"
#include "Material.h"

class Reflective : public Lambert
{
public:
    Reflective(	const Vector3 & kd = Vector3(1),
				const Vector3 & ka = Vector3(0),
				const Vector3 & ks = Vector3(0),
				const float phongexp = 64 );
    virtual ~Reflective();
    
    const Vector3 & ks() const {return m_ks;}
    
    void setKs(const Vector3 & ks) {m_ks = ks;}
	
	virtual void preCalc() {}    
    virtual Vector3 shade(const Ray& ray, const HitInfo& hit,
                          const Scene& scene) const;
protected:
    Vector3 m_ks;  
	float phong_exponent;
};

#endif // CSE168_LAMBERT_H_INCLUDED
