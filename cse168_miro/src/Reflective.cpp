#include "Reflective.h"
#include "Ray.h"
#include "Scene.h"

Reflective::Reflective(const Vector3 & kd, const Vector3 & ka, const Vector3 & ks, const float phongexp)	
{
	m_kd = kd;
	m_ka = ka;
	m_ks = ks;
	phong_exponent = phongexp;
}

Reflective::~Reflective()
{
}

Vector3
Reflective::shade(const Ray& ray, const HitInfo& hit, const Scene& scene) const
{
	HitInfo tempHit;
	tempHit.m_lightIntensity = hit.m_lightIntensity;
	Ray ray2nd;
	ray2nd.o = hit.P;
    Vector3 L = Vector3(0.0f, 0.0f, 0.0f);    
	
	float nDotRay = dot(-ray.d, hit.N);
	Vector3 idealReflect = 2*nDotRay*hit.N + ray.d;	

    const Lights *lightlist = scene.lights();

    
    // loop over all of the lights
    Lights::const_iterator lightIter;
    for (lightIter = lightlist->begin(); lightIter != lightlist->end(); lightIter++)
    {
        Light* pLight = *lightIter;

        Vector3 l = pLight->position() - hit.P;
		
		//hard shadow
		pLight->getShadowRay(hit.P, ray2nd);						
		if( scene.trace(tempHit, ray2nd, epsilon)){
			if(l.length2() > (tempHit.P-ray2nd.o).length2())
				continue;
		}
		ray2nd.o = hit.P;

        // the inverse-squared falloff
        float falloff = l.length2();
        
        // normalize the light direction
        l /= sqrt(falloff);

        // get the diffuse component
        float nDotL = dot(hit.N, l);
        Vector3 result = pLight->color();        

		float energy = std::max(0.0f, nDotL * pLight->wattage() / (4.0f * PI * falloff));
        
        L += result * energy * m_kd;

		float phong = dot(idealReflect, l);   
        if(phong > epsilon && nDotRay > 0) // episilon check gets rid of "tangent phong highlight" and internal phong shading
		{			
            L += result * (pow(phong,phong_exponent) /( nDotL )) * energy * m_ks;//calc the phong highlight 
		}
    }
    

	//100% of the specular light is reflective
	ray2nd.d = idealReflect;//.normalized();
	float intensity = (Vector3(1.0) - m_ks).length() / (Vector3(1.0).length());	
	if( hit.m_lightIntensity > 0.001)
    {
      if(scene.trace(tempHit, ray2nd, epsilon))
        {		
            tempHit.m_lightIntensity = intensity * hit.m_lightIntensity;		
            L += tempHit.material->shade(ray2nd, tempHit, scene) * m_ks ;		
        }else{
            L += scene.findEnvCoords(ray2nd.d) * m_ks;
		}
    }
	
	

	/*
	reflectDepth++;
	if (reflectDepth < reflectMax && scene.trace(tempHit, ray2nd, epsilon))
	{
		tempHit.m_lightIntensity *= m_ks.length();
		L += tempHit.material->shade(ray2nd, tempHit, scene) * m_ks;
		tempHit.m_lightIntensity = hit.m_lightIntensity;
	}
	reflectDepth--;
	*/
    // add the ambient component
    L += m_ka;
    
    return L;
}

