#include "StoneTexture.h"
#include "Ray.h"
#include "Scene.h"


StoneTexture::StoneTexture() 
{

}

StoneTexture::~StoneTexture()
{

}

float perlin_turbulence(Vector3 pos, int level)
{    	
	float turb = 0.0;
	float freq = 1;
    for(float i = 1.0; i < level+1; i++ )
    {		
        turb += (PerlinNoise::noise(pos.x*freq, pos.y*freq, pos.z*freq) / freq);
		freq *= 2;
    }	
    return turb;

}
#define MAX_ORDER 5
float worley_turbulence(Vector3 pos, float *F, unsigned long *ID, int cycles)
{
	float worleyPos[] = {pos.x, pos.y, pos.z};        
    float worleyF[MAX_ORDER];
    float worleyDelta[MAX_ORDER][3];        
    for(int i = 0; i < MAX_ORDER; i++)
    {
        F[i]=0;//zero out everything
    }
	for(int i = 0; i <cycles; i++ )
    {
        worleyPos[0] = pos.x*i;
        worleyPos[1] = pos.y*i;
        worleyPos[2] = pos.z*i;    
        WorleyNoise::noise3D(worleyPos, MAX_ORDER, worleyF, worleyDelta, ID); 
        for(int j = 0;j  < MAX_ORDER; ++j)
        {
           F[j] += (worleyF[j]) / i;
        }
    }    
}
#define NUM_COLORS 4
Vector3 StoneTexture::color_picker(int id) const
{
	Vector3 retColor;
	int temp = id % NUM_COLORS;
	switch( temp )
	{
	case 0:
		return Vector3(138/255.0,118/255.0,93/255.0);
	case 1:
		return Vector3(106/255.0,73/255.0,44/255.0);
	case 2:
		return Vector3(166/255.0,142/255.0,132/255.0);
	case 3:
		return Vector3(199/255.0,178/255.0,153/255.0);
	}	
	return Vector3(0); // return black as default
}

Vector3
StoneTexture::shade(const Ray& ray, const HitInfo& hit, const Scene& scene) const
{
	HitInfo tempHit;
	Ray ray2nd;
	ray2nd.o = hit.P;
    Vector3 L = Vector3(0.0f, 0.0f, 0.0f);

    
	float p_noise = perlin_turbulence(hit.P, 5);

	//WORLEY
	Vector3 color = Vector3(0.0f, 0.0f, 0.0f);
    
	unsigned long *ID = new unsigned long[2];
	float *F = new float[2];
	float (*delta)[3] = new float[2][3];
	float *at = new float[3];
	at[0] = hit.P.x;
	at[1] = hit.P.y;
	at[2] = hit.P.z;

	WorleyNoise::noise3D(at,2,F,delta,ID);
	//WORLEY!

	float distance = (F[1]-F[0]);
	//distance *= (distance);

	int colorID = ID[0];

    const Lights *lightlist = scene.lights();	

	Vector3 p = hit.P;
	
	tempHit.P = hit.P;
	if(distance<0.05f){
		float fnoise = perlin_turbulence(p*8,8);
		float noise = (1+fnoise)/2;
		
		tempHit.N += noise;
		color =  Vector3(.2, .2, .2)+noise;
	}else{
		float fnoise = perlin_turbulence(p*2,3);
		float noise = (1+fnoise)/2;
		
		tempHit.P += tempHit.N*noise;
		color = color_picker(abs(colorID)) * noise;
	}


    const Vector3 viewDir = -ray.d; // d is a unit vector	
    
    // loop over all of the lights
    Lights::const_iterator lightIter;
    for (lightIter = lightlist->begin(); lightIter != lightlist->end(); lightIter++)
    {
        Light* pLight = *lightIter;
    
        Vector3 l = pLight->position() - hit.P;
        
		ray2nd.d = l.normalized();

		//hard shadow
		pLight->getShadowRay(hit.P, ray2nd);						
		if( scene.trace(tempHit, ray2nd, epsilon)){
			if(l.length2() > (tempHit.P-ray2nd.o).length2())
				continue;
		}
		ray2nd.o = hit.P;

        // the inverse-squared falloff
        float falloff = l.length2();
        
        // normalize the light direction
        l /= sqrt(falloff);

        // get the diffuse component
        float nDotL = dot(hit.N, l);
        Vector3 result = pLight->color();
        result *= color;
        
        L += std::max(0.0f, nDotL/falloff * pLight->wattage() / (4.0f * PI)) * result;
    }
    
    // add the ambient component
    L += Vector3(0.05);

	//L += PerlinNoise::noise(hit.P.x, hit.P.y, hit.P.z);
    
    return L;
}
