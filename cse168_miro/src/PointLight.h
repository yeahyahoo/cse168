#ifndef CSE168_POINTLIGHT_H_INCLUDED
#define CSE168_POINTLIGHT_H_INCLUDED

#include "Light.h"
#include <vector>
#include "Vector3.h"

class PointLight : public Light
{
public:
    /*void setPosition(const Vector3& v)  {m_position = v;}
    void setColor(const Vector3& v)     {m_color = v;}
    void setWattage(float f)            {m_wattage = f;}
    	
    float wattage() const               {return m_wattage;}
    const Vector3 & color() const       {return m_color;}
    const Vector3& position() const     {return m_position;}

	float calcShadow(Ray);

    void preCalc() {} // use this if you need to
	*/
	virtual void getStratifiedSamples(Ray* ray, int& u_samples, int& v_samples)
	{				
		float u_interval = 1.0f/u_samples;
		float v_interval = 1.0f/v_samples;		
		float x,y,z;
		float theta,phi;
		for(int i = 0; i < u_samples; i++)
		{
			for(int j = 0; j < v_samples; j++)
			{
				
				theta = PI*rand()/(float)RAND_MAX;
				phi = 2*PI*rand()/(float)RAND_MAX;
				
				x = sin(theta)*cos(phi);	
				y =	sin(theta)*sin(phi);	
				z = cos(theta);	

				ray[i*u_samples + j].o = m_position;
				ray[i*u_samples + j].d = Vector3(x,y,z).normalized();
			}
		}
	}
	virtual void getSample(Ray& ray)
	{
		float x,y,z;
		do{
			x = 1-2*(rand()/(float)RAND_MAX);
			y = 1-2*(rand()/(float)RAND_MAX);
			z = 1-2*(rand()/(float)RAND_MAX);
		}while(sqrt(x*x+y*y+z*z) > 1);

		Vector3 tempPos(x,y,z);
		tempPos.normalize();
		ray.o = tempPos+m_position;
		ray.d = tempPos;
	}
	virtual void getStratifiedShadowRay(const Vector3 hitpos, Ray* ray, int& u_samples, int& v_samples)
	{
		for(int i = 0; i < u_samples; i++)
		{
			for(int j = 0; i < v_samples; j++)
			{
				ray[i*u_samples + j].o = m_position;
				ray[i*u_samples + j].d = (hitpos-m_position).normalized();
			}
		}

		/*
		ray.o = m_position;
		ray.d = hitpos-m_position;
		Ray newRay;
		Vector3 normal = (hitpos - m_position).normalized();
		Vector3 v,u, global;
		v = Vector3(1.0f,0.0f,0.0f); // temporary v (aka v1)
		u = cross(normal,v);
		if(dot(u,u) < epsilon) // vec u is same as vec v1
		{
	        v = Vector3(0.0f,1.0f,0.0f); // temporary v (aka v2)
			u = cross(normal,v);
		}
		v = cross(u,normal);
		v.normalize();
		u.normalize();

		float u_interval = 1.0f/u_samples;
		float v_interval = 1.0f/v_samples;		
		for(int i = 0; i < u_samples; i++)
		{
			for(int j = 0; j < v_samples; j++)
			{
				// currently not stratified
				float phi = 2*PI*((i/u_samples + u_interval*rand()/(float)RAND_MAX)); // gen a rand phi 0->2PI degrees
				float theta = PI*((j/v_samples + v_interval*rand()/(float)RAND_MAX)); // gen a rand theta 0->2PI degrees

				Vector3 tempPos =	sin(theta)*cos(phi)*u +
									sin(theta)*sin(phi)*v;

				ray[i*u_samples + j].o = tempPos+m_position;
				ray[i*u_samples + j].d = (hitpos-ray[i*u_samples + j].o).normalized();
			}
		}
		*/

	}
	virtual void getShadowRay(const Vector3 hitpos, Ray& ray)
	{
		ray.o = hitpos;
		ray.d = (m_position-hitpos).normalized();
	}
	virtual Vector3 positionShadow(Vector3 hitpos){
		return m_position;
	}
protected:
};

//typedef std::vector<PointLight*> Lights;

#endif // CSE168_POINTLIGHT_H_INCLUDED

