#ifndef CSE168_AABB_H_INCLUDED
#define CSE168_AABB_H_INCLUDED

#include "Vector3.h"
#include "Object.h"
#include "Ray.h"

class AABB : public Object
{
public:
	AABB();
	AABB(Objects * objs);
    AABB(Vector3, Vector3);
    virtual ~AABB();

    virtual void renderGL();
    virtual bool intersect(HitInfo& result, const Ray& ray, float tMin = 0.0f, float tMax = MIRO_TMAX);
	virtual void packetIntersect(RayPacket& rayPacket, std::vector<bool>& mask, 
								float tMin = 0.0f, float tMax = MIRO_TMAX);
	void packetIntersect(HitInfo result[4],
						 Ray rayInfo[4],
						 bool mask[4],
						 float tMin = 0.0f, float tMax = MIRO_TMAX);

	virtual void getAABB(Vector3& min, Vector3& max) {min = a; max = b;}
	virtual bool isAABB(){return true;}

	Vector3 getCenter();
	
	float getSurfaceArea();
	float getVolume();

	void fourRayIntersectBox(const Ray ray[4], float t[4], float check[4]);
	bool intersectBox(const Ray& ray, float& t);

	static long unsigned int num_ray_box;
	static void resetCounters();

public:
	Vector3 a; //min 
	Vector3 b; //max
	bool isLeaf;

	/*AABB* left;
	AABB* right;*/
	Objects *objects;
};

#endif