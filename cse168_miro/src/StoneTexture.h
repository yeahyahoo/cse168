#ifndef _STONE_TEXTURE_H_
#define _STONE_TEXTURE_H_

#include "Material.h"
#include "Worley.h"
#include "Perlin.h"

class StoneTexture : public Material
{
public:
    StoneTexture( );
    virtual ~StoneTexture();	

    virtual void preCalc() {}
    
	Vector3 color_picker(int id) const;

    virtual Vector3 shade(const Ray& ray, const HitInfo& hit,
                          const Scene& scene) const;
protected:	

};

#endif // CSE168_LAMBERT_H_INCLUDED
