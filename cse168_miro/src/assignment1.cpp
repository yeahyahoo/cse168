#include "assignment1.h"
#include <math.h>
#include "Miro.h"
#include "Scene.h"
#include "Camera.h"
#include "Image.h"
#include "Console.h"

#include "Sphere.h"
#include "PointLight.h"
#include "SphereLight.h"
#include "AreaLight.h"
#include "TriangleMesh.h"
#include "Triangle.h"
#include "Lambert.h"
#include "Reflective.h"
#include "Refractive.h"
#include "StoneTexture.h"

void
makeBunnyScene()
{
    g_camera = new Camera;
    g_scene = new Scene;
    g_image = new Image;

    g_image->resize(256, 256);	
    
    // set up the camera
    g_camera->setBGColor(Vector3(0.0f, 0.0f, 0.2f));
    g_camera->setEye(Vector3(-2, 3, 5));
    g_camera->setLookAt(Vector3(-.5, 1, 0));
    g_camera->setUp(Vector3(0, 1, 0));
    g_camera->setFOV(45);

    // create and place a point light source
	/*
	PointLight * light = new PointLight;
    light->setPosition(Vector3(-3, 15, 3));
    light->setColor(Vector3(1, 1, 1)); 
    light->setWattage(1000);
    g_scene->addLight(light);
	*/

    PointLight * light = new PointLight;
    light->setPosition(Vector3(-3, 15, 3));
    light->setColor(Vector3(0, 1, 1)); //cyan
    light->setWattage(1000);
    g_scene->addLight(light);

	light = new PointLight;
    light->setPosition(Vector3(-5, 15, 5));
    light->setColor(Vector3(1, 0, 1)); // magenta
    light->setWattage(1000);
    g_scene->addLight(light);

	light = new PointLight;
    light->setPosition(Vector3(-3, 15, 10));
    light->setColor(Vector3(1, 1, 0)); // yellow
    light->setWattage(1000);
    g_scene->addLight(light);
	
    Material* mat = new Lambert(Vector3(0.6f), 
								Vector3(0.05f));
	Material* shiny =  new Reflective(	Vector3(0.2),
										Vector3(0.0),
										Vector3(0.6));

    TriangleMesh * bunny = new TriangleMesh;    
	//bunny->load("../cse168_miro/resource/teapot.obj");
	//bunny->load("../cse168_miro/resource/bunny.obj");
	bunny->load("../cse168_miro/resource/bunny_smooth.obj");
	
    // create all the triangles in the bunny mesh and add to the scene
    for (int i = 0; i < bunny->numTris(); ++i)
    {
        Triangle* t = new Triangle;
        t->setIndex(i);
        t->setMesh(bunny);
        t->setMaterial(shiny); 
        g_scene->addObject(t);
    }
    // create the floor triangle
    TriangleMesh * floor = new TriangleMesh;
    floor->createSingleTriangle();
    floor->setV1(Vector3(  0, 0,  10));
    floor->setV2(Vector3( 10, 0, -10));
    floor->setV3(Vector3(-10, 0, -10));
    floor->setN1(Vector3(0, 1, 0));
    floor->setN2(Vector3(0, 1, 0));
    floor->setN3(Vector3(0, 1, 0));
    
    Triangle* t = new Triangle;
    t->setIndex(0);
    t->setMesh(floor);
    t->setMaterial(mat); 
    g_scene->addObject(t);
    
    // let objects do pre-calculations if needed
	g_scene->loadEnvironmentMap("../cse168_miro/resource/stpeters_probe.pfm");
    g_scene->preCalc();
}
/*
void
makeTeapotScene()
{
    g_camera = new Camera;
    g_scene = new Scene;
    g_image = new Image;

    g_image->resize(128, 128);
    
    // set up the camera
    g_camera->setBGColor(Vector3(0.0f, 0.0f, 0.2f));
    g_camera->setEye(Vector3(-2, 3, 5));
    g_camera->setLookAt(Vector3(-.5, 1, 0));
    g_camera->setUp(Vector3(0, 1, 0));
    g_camera->setFOV(45);

    // create and place a point light source
    PointLight * light = new PointLight;
    light->setPosition(Vector3(-3, 15, 3));
    light->setColor(Vector3(0, 1, 1)); //cyan
    light->setWattage(1000);
    g_scene->addLight(light);

	light = new PointLight;
    light->setPosition(Vector3(-5, 15, 5));
    light->setColor(Vector3(1, 0, 1)); // magenta
    light->setWattage(1000);
    g_scene->addLight(light);

	light = new PointLight;
    light->setPosition(Vector3(-3, 15, 10));
    light->setColor(Vector3(1, 1, 0)); // yellow
    light->setWattage(1000);
    g_scene->addLight(light);



    Material* mat = new Lambert(Vector3(1.0f));
	Material* red = new Lambert(Vector3(0.9f, 0, 0));
	Material* refract = new Refractive( Vector3(0.2),
									 Vector3(0.0),
									 Vector3(0.6),
									 16,
									 1.3);

    TriangleMesh * teapot = new TriangleMesh;    
	teapot->load("../cse168_miro/resource/teapot.obj");
	
    
    // create all the triangles in the teapot mesh and add to the scene
    for (int i = 0; i < teapot->numTris(); ++i)
    {
        Triangle* t = new Triangle;
        t->setIndex(i);
        t->setMesh(teapot);
        t->setMaterial(refract); 
        g_scene->addObject(t);
    }
    
    // create the floor triangle
    TriangleMesh * floor = new TriangleMesh;
    floor->createSingleTriangle();
    floor->setV1(Vector3(  0, 0,  10));
    floor->setV2(Vector3( 10, 0, -10));
    floor->setV3(Vector3(-10, 0, -10));
    floor->setN1(Vector3(0, 1, 0));
    floor->setN2(Vector3(0, 1, 0));
    floor->setN3(Vector3(0, 1, 0));
    
    Triangle* t = new Triangle;
    t->setIndex(0);
    t->setMesh(floor);
    t->setMaterial(red); 
    g_scene->addObject(t);
    
    // let objects do pre-calculations if needed
    g_scene->preCalc();
}
*/
void
makeCornellScene()
{
    g_camera = new Camera;
    g_scene = new Scene;
    g_image = new Image;

    g_image->resize(256, 256);
    
    // set up the camera
    g_camera->setBGColor(Vector3(0.0f, 0.0f, 0.2f));
    g_camera->setEye(Vector3(2, 3, 7));
    g_camera->setLookAt(Vector3(2.5, 2.5, 0));
    g_camera->setUp(Vector3(0, 1, 0));
    g_camera->setFOV(45);

    // create and place a point light source
    AreaLight * light = new AreaLight;
    light->setPosition(Vector3(2.75, 5, -2.5));
    light->setColor(Vector3(1, 1, 1)); //cyan
    light->setWattage(140);
	light->setU(Vector3(2,0,0));
	light->setV(Vector3(0,0,2));
    g_scene->addLight(light);
	/*
	light = new PointLight;
    light->setPosition(Vector3(-15, 15, 5));
    light->setColor(Vector3(1, 1, 1)); // magenta
    light->setWattage(1000);
    g_scene->addLight(light);

	light = new PointLight;
    light->setPosition(Vector3(-15, 15, 16));
    light->setColor(Vector3(1, 1, 1)); // yellow
    light->setWattage(1000);
    g_scene->addLight(light);
	*/
	Material* mat = new Lambert(	Vector3(0.5f, 0.5f, 0.5f), 
										Vector3(0.05) );
    
	Material* shiny = new Reflective(	Vector3(0.3f, 0.3f, 0.3f), 
										Vector3(0.05),
										Vector3(.6));
	
	Material* refract = new Refractive(	Vector3(0.3, 0.3, 0.3),
										Vector3(0.05),
										Vector3(0.6),
										16,
										1.3); // ice

    TriangleMesh * box = new TriangleMesh;    
	box->load("../cse168_miro/resource/cornell_box.obj");
	
    
    // create all the triangles in the box mesh and add to the scene
    for (int i = 0; i < box->numTris(); ++i)
    {
        Triangle* t = new Triangle;
        t->setIndex(i);
        t->setMesh(box);
        t->setMaterial(mat); 
        g_scene->addObject(t);
    }
    
    // create the floor triangle
    TriangleMesh * floor = new TriangleMesh;
    floor->createSingleTriangle();
	floor->setV1(Vector3(  0, -0.11,  10));
    floor->setV2(Vector3( 10, -0.11, -10));
    floor->setV3(Vector3(-10, -0.11, -10));	
    floor->setN1(Vector3(0, 1, 0));
    floor->setN2(Vector3(0, 1, 0));
    floor->setN3(Vector3(0, 1, 0));
    
    Triangle* t = new Triangle;
    t->setIndex(0);
    t->setMesh(floor);
    t->setMaterial(mat); 
    g_scene->addObject(t);
    

	g_scene->loadEnvironmentMap("../cse168_miro/resource/stpeters_probe.pfm");
    // let objects do pre-calculations if needed
    g_scene->preCalc();
}

void makeSphereScene(){

	g_camera = new Camera;
	g_scene = new Scene;
	g_image = new Image;

	g_image->resize(512, 512);

	// set up the camera
	g_camera->setBGColor(Vector3(1.0f, 1.0f, 1.0f));
	g_camera->setEye(Vector3(-6, 9, 15));
	g_camera->setLookAt(Vector3(-.5, 0, 0));
	g_camera->setUp(Vector3(0, 1, 0));
	g_camera->setFOV(45);

	// create and place a point light source
	PointLight * light = new PointLight;
	light->setPosition(Vector3(-6, 15, 6));
	light->setColor(Vector3(1, 1, 1));
	light->setWattage(1000);
	g_scene->addLight(light);

	light = new PointLight;
	light->setPosition(Vector3(-8, 15, 8));
	light->setColor(Vector3(1, 1, 1));
	light->setWattage(1000);
	g_scene->addLight(light);

	light = new PointLight;
	light->setPosition(Vector3(-8, 15, 6));
	light->setColor(Vector3(1, 1, 1));
	light->setWattage(1000);
	g_scene->addLight(light);

    Material* green = new Lambert(Vector3(0,1,0));
	Material* red = new Lambert(Vector3(1,0,0));
	Material* reflect = new Reflective( Vector3(0.4),
										  Vector3(0), 
										  Vector3(0.9));
	Material* refract = new Refractive( Vector3(0.4),
										Vector3(0), 
										Vector3(0.8),
										16,
										1.3);
	Material* stone = new StoneTexture();

	//matSphere->setRefractiveIndex(1.4);

	Sphere * sphere = new Sphere;
	sphere->setCenter(Vector3(-4,2.2,0));
	sphere->setRadius(2);
	sphere->setMaterial(reflect);
	g_scene->addObject(sphere);

	sphere = new Sphere;
	sphere->setCenter(Vector3(0,2.2,4));
	sphere->setRadius(2);
	sphere->setMaterial(refract);
	g_scene->addObject(sphere);

	sphere = new Sphere;
	sphere->setCenter(Vector3(3,2.2,-3));
	sphere->setRadius(2);
	sphere->setMaterial(refract);
	g_scene->addObject(sphere);
	
    // create the floor
    TriangleMesh * floor = new TriangleMesh;
    floor->createSingleTriangle();
    floor->setV1(Vector3(-10, 0,  10));
    floor->setV2(Vector3( 10, 0, -10));
    floor->setV3(Vector3(-10, 0, -10));

    floor->setN1(Vector3(0, 1, 0));
    floor->setN2(Vector3(0, 1, 0));
    floor->setN3(Vector3(0, 1, 0));
    
    Triangle* t = new Triangle;
    t->setIndex(0);
    t->setMesh(floor);
    t->setMaterial(stone); 
    g_scene->addObject(t);
    
    // create the floor triangle
    TriangleMesh * floor2 = new TriangleMesh;
    floor2->createSingleTriangle();
    floor2->setV1(Vector3(-10, 0,  10));
    floor2->setV2(Vector3( 10, 0, 10));
    floor2->setV3(Vector3(10, 0, -10));

    floor2->setN1(Vector3(0, 1, 0));
    floor2->setN2(Vector3(0, 1, 0));
    floor2->setN3(Vector3(0, 1, 0));
    
    Triangle* t2 = new Triangle;
    t2->setIndex(0);
    t2->setMesh(floor2);
    t2->setMaterial(stone); 
    g_scene->addObject(t2);
	

	// let objects do pre-calculations if needed
	g_scene->loadEnvironmentMap("../cse168_miro/resource/stpeters_probe.pfm");
	g_scene->preCalc();

}