#ifndef CSE168_BVH_H_INCLUDED
#define CSE168_BVH_H_INCLUDED


#include "Miro.h"
#include "Object.h"
#include "AABB.h"
#include "Ray.h"

class BVH
{
public:
	BVH();
    void build(Objects * objs);

    bool intersect(HitInfo& result, const Ray& ray,
                   float tMin = 0.0f, float tMax = MIRO_TMAX) const;
	void packetIntersect( RayPacket& rayPacket,
					float tMin = 0.0f, float tMax = MIRO_TMAX) const;
	AABB* subdivide_node(AABB *node, Objects *objList);
	void drawBoxes();

	bool usingBVH(){return useBVH;}

	static long unsigned int num_BVHNodes;
	static long unsigned int num_BVHLeaves;
protected:
    Objects * m_objects;
	AABB* rootBox;
	int trianglesPerBox;
	bool useBVH;
};

#endif // CSE168_BVH_H_INCLUDED