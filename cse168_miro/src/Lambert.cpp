#include "Lambert.h"
#include "Ray.h"
#include "Scene.h"

Lambert::Lambert(const Vector3 & kd, const Vector3 & ka) :
    m_kd(kd), m_ka(ka)
{

}

Lambert::~Lambert()
{
}

Ray Lambert::getHemisphereDirectSample(Vector3 pos, Vector3 normal) const
{

    Ray newRay;
    Vector3 v,u, global;
    v = Vector3(1.0f,0.0f,0.0f); // temporary v (aka v1)
	u = cross(normal,v);
    if(dot(u,u) < epsilon) // vec u is same as vec v1
    {
        v = Vector3(0.0f,1.0f,0.0f); // temporary v (aka v2)
		u = cross(normal,v);
    }
	v = cross(u,normal);
	v.normalize();
	u.normalize();

    int maxDiffuseRays = 1;
	newRay.o = pos;

	float phi = 2*PI*(rand()/(float)RAND_MAX); // gen a rand phi 0->2PI degrees
    float theta = 2*PI*(rand()/(float)RAND_MAX); // gen a rand theta 0->PI degrees
        
	newRay.d =  sin(theta)*cos(phi)*v +
				sin(theta)*sin(phi)*u +
				cos(theta)*normal;

	return newRay;
	
	/*
	float u1 = rand()/(float)RAND_MAX;
	float u2 = rand()/(float)RAND_MAX;
	const float r = sqrt(u1);
	const float angle = 2 * PI * u2;
	const float x = r * cos(angle);
	const float y = r * sin(angle);
	return Vector3(x, y, sqrt(std::max(0.0f, 1 - u1)));
	*/
}


Vector3
Lambert::shade(const Ray& ray, const HitInfo& hit, const Scene& scene) const
{
	HitInfo tempHit;
	Ray ray2nd;
	ray2nd.o = hit.P;
    Vector3 L = Vector3(0.0f, 0.0f, 0.0f);
    
    const Vector3 viewDir = -ray.d; // d is a unit vector
    
    const Lights *lightlist = scene.lights();
    
    // loop over all of the lights
    Lights::const_iterator lightIter;	
    for (lightIter = lightlist->begin(); lightIter != lightlist->end(); lightIter++)
    {
        Light* pLight = *lightIter;
    
		Vector3 l = pLight->positionShadow(hit.P) - hit.P;
        
		//hard shadow
		ray2nd.d = l.normalized();
		if( scene.trace(tempHit, ray2nd, epsilon)){
			if(l.length2() > (tempHit.P-hit.P).length2())
				continue;
		}		
        
        // the inverse-squared falloff
        float falloff = l.length2();
        
        // normalize the light direction
        l /= sqrt(falloff);

        // get the diffuse component
        float nDotL = dot(hit.N, l);
        Vector3 result = pLight->color();
        result *= m_kd;
        
        L += std::max(0.0f, nDotL/falloff * pLight->wattage() / (4.0f * PI)) * result;
    }
    


    // add the ambient component
    L += m_ka;		
	// indirect lighting	
	float intensity = (Vector3(1.0) - m_kd).length() / (Vector3(1.0).length());	
	if(hit.m_lightIntensity > 0.1)
    {		
		ray2nd = getHemisphereDirectSample(hit.P, hit.N.normalized());
		if(scene.trace(tempHit, ray2nd, epsilon))
        {		
            tempHit.m_lightIntensity = intensity * hit.m_lightIntensity;			
            L += tempHit.material->shade(ray2nd, tempHit, scene) * m_kd ;		
        }else{
            //L += scene.findEnvCoords(ray2nd.d) * m_kd;
		}
    }
	

    
    return L;
}
